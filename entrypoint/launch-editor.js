#!/usr/bin/env node

const sfs = require('@stele1/filesystem');
const runServer = require('../server/index.js').default;

const path = require('path');
const fs = require('fs');

const readArgs = function(args){
    let i = 2;
    let opts = {};
    while (i < args.length) {
        switch (args[i]) {
            case '-port':
                opts.port = 1 * args[i+1];
                i += 2;
                break;
            case '-h':
            case '-help':
            case '--help':
                opts.help = true
                i += 1;
                break;
            case '-static-files':
                opts.staticFiles = args[i+1];
                i += 2;
                break;
            case '-address':
                opts.address = args[i+1];
                i += 2;
                break;
            default:
                // TODO: check if this is a directory 
                // TODO: check if this is set twice
                opts.projectPath = args[i];
                i += 1;
                break;
        }
    }
    return opts;
};

const help = `usage: ${process.argv[0]} [OPTIONS] STELE1_PROJECT_PATH

   -address      ADDR               (default 127.0.0.1)
   -port         PORT               (default 8001)
   -static-files STATIC_PATH
   -help

run an editor for STELE1_PROJECT at localhost:PORT with files from STATIC_PATH`;

// 
// This behavior is simple but not powerful.
// It's predictable and minimizes unintentional filesystem modification.
//
// file exists and directory?
//     empty?                  --> create a project
//     not empty?              --> existing project
// nothing?                    --> error
// 
function handlePath(a){
    if (fs.existsSync(a) && fs.statSync(a).isDirectory()) {
      if (fs.readdirSync(a).length) { 
          return new sfs.Project(a);
      } else {
          const p = new sfs.Project(a);
          p.create();
          return p;
      }
    } else {
      throw Error(`path ${a} must be a directory`);
    }
}

function main(){
    const cfg = Object.assign({
        address: '127.0.0.1',
        port: 8001,
        staticFiles: path.join(path.dirname(__dirname), 'client'),
        help: false,
        projectPath: './',
    }, readArgs(process.argv));
    const proj = handlePath(cfg.projectPath);
    if (cfg.help) {
        console.log(help);
        process.exit(0);
    }
    if (!proj.exists()) {
        throw Error('can not find stele1 project at ' + cfg.projectPath)
    }
    runServer(proj, cfg.staticFiles, cfg.address, cfg.port);
    console.log('running editor for %s on %s:%s', cfg.projectPath, cfg.address, cfg.port);
}

main();

