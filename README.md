# the montserrat editor for stele1

It's somewhat cobbled together, and not quite _good_ yet.

You can, however, edit _stele1_ projects.

Run the server locally and edit projects stored on your filesystem.

## use

This runs on Linux, macOS, and Docker.

## Containers (Podman or Docker)

1. **install docker**
2. **pull an image.**
   development images are available from the website for _stele1_.
3. **load the image** into docker
4. **run the image** to edit projects on your filesystem

```sh
$ wget http://stele1.steleclimbing.org/tmp/montserrat-editor_alpha-02_docker.tar
$ docker load < montserrat-editor_alpha-02_docker.tar
$ mkdir ./climbingarea.stele1.d/
$ docker run --rm \
    --publish 8001:8001 \
    --volume ./climbingarea.stele1.d/:/srv/project.stele1.d/ \
    stele1/montserrat-editor:alpha-02 
```

## Linux

1. Install _Node.js_ and _ImageMagick_.
2. Install [_stele1_](https://codeberg.org/stele-climbing/stele1)
3. Clone this project.
<!-- TODO: expand the instructions -->

edit existing projects:

```sh
$ montserrat-editor ~/path/to/yosemite.stele1.d/
$ firefox localhost:8001
```

create (and edit) new projects:

```sh
$ mkdir ~/my-new-project.stele1.d/
$ montserrat-editor ~/my-new-project.stele1.d/
$ firefox localhost:8001
```

<!--
TODO: proper installation instructions on linux
TODO: proper installation instructions on docker
-->

## status
