## developer notes

- Make the photo-climb-overlay editor draggable
- clear the photo cache when a photo's realOrientation is changed
- **more snapping** -
  the way OSM features snap to each other helps with cohesion.
  under the current approach, it's near impossible to get a particular level of refinement
  - an approach to start _on_ a parking area
  - a trail to start _on_ a parking area
  - two climbs (supposing one is a direct finish)
     to share the exact same points until they diverge
