
pages.Project = {
    oninit: function(vnode){
        this.metadata = null;
        m.request('/metadata').then(res => { this.metadata = res; });
        document.title = 'Metadata';
    },
    save: util.debounce(1000, function(){
        for (let k in this.metadata)
            if (this.metadata[k] === null)
                delete this.metadata[k];
        m.request({
            method: 'PUT', 
            url: '/metadata',
            body: this.metadata
        });
    }),
    view: function() {
        let md = this.metadata;
        if (!md) return m('p', 'loading...');
        return m('article.standard', [
            m('h1', 'Edit Project Metadata'),
            m('table', [
                m('tr', [
                    m('td', 'name'),
                    m('td', [
                        m(components.project.Name, {
                            value: md.name,
                            onchange: x => { md.name = x; this.save(); },
                        }),
                    ]),
                ]),
                m('tr', [
                    m('td', 'description'),
                    m('td', [
                        m(components.project.Description, {
                            value: md.description,
                            onchange: x => { md.description = x; this.save(); },
                        }),
                    ]),
                ]),
                m('tr', [
                    m('td', 'authors'),
                    m('td', [
                        m(components.project.Authors, {
                            value: md.authors,
                            onchange: x => { md.authors = x; this.save(); },
                        }),
                    ]),
                ]),
                m('tr', [
                    m('td', 'uuid'),
                    m('td', m(components.project.UUID, {value: md.uuid})),
                ]),
            ]),
        ]);
    }
};
