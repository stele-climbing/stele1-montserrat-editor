(function(window, document){

'use strict';

class MapPool {
    constructor(element){
        this.map = L.prebuilt.maps.empty(element);
        this.groups = {
            climbs: L.layerGroup(),
            areas: L.layerGroup(),
            photos: L.layerGroup(),
            photoExifs: L.layerGroup(),
            approaches: L.layerGroup(),
            parkings: L.layerGroup(),
        };
        this.tiles = {
            trails: L.prebuilt.layers.trails(),
            streets: L.prebuilt.layers.streets(),
            satellite1: L.prebuilt.layers.satellite(),
            satellite2: L.prebuilt.layers.hereSatellite(),
            hybrid: L.prebuilt.layers.hereHybrid(),
            topo: L.prebuilt.layers.topo(),
            bing: L.prebuilt.layers.bing(),
        };
        const baseSelect = new L.Mount.BaseLayerSelect();
        this.map.mount(baseSelect);
        this.map.addLayer(this.tiles.bing);
        baseSelect
            .manageLayer({title: "bing",        layer: this.tiles.bing})
            .manageLayer({title: "streets",     layer: this.tiles.streets})
            .manageLayer({title: "satellite 1", layer: this.tiles.satellite1})
            .manageLayer({title: "satellite 2", layer: this.tiles.satellite2})
            .manageLayer({title: "hybrid",      layer: this.tiles.hybrid})
            .manageLayer({title: "topo",        layer: this.tiles.topo})
            .manageLayer({title: "trails",      layer: this.tiles.trails});
        this.markers = {
            climbs: {},
            areas: {},
            photos: {},
            photoExifs: {},
            approaches: {},
            parkings: {},
        };
    }
    getClimbMarker(c){
        const cm = this.markers.climbs;
        if (!c.location) return null;
        cm[c.uuid] = cm[c.uuid] || common.markers.climb(c.location, c);
        return cm[c.uuid];
    }
    addClimbMarker(c){
        this.getClimbMarker(c)?.addTo(this.groups.climbs);
    }
    setClimbVisibility(show){
        this.map[show ? 'addLayer' : 'removeLayer'](this.groups.climbs);
    }
    getPhotoMarker(p){
        if (!p.realLocation) return null;
        const pm = this.markers.photos;
        pm[p.uuid] = pm[p.uuid] || common.markers.photo(p.realLocation, p);
        return pm[p.uuid];
    }
    addPhotoMarker(p){
        this.getPhotoMarker(p)?.addTo(this.groups.photos);
    }
    setPhotoVisibility(show){
        this.map[show ? 'addLayer' : 'removeLayer'](this.groups.photos);
    }
    getPhotoExifMarker(point, photo){
        const pem = this.markers.photoExifs;
        if (!point) return null;
        pem[photo.uuid] = pem[photo.uuid] || common.markers.photoExif(point, photo);
        return pem[photo.uuid];
    }
    addPhotoExifMarker(point, photo){
        this.getPhotoExifMarker(point, photo)?.addTo(this.groups.photoExifs);
    }
    setPhotoExifVisibility(show){
        this.map[show ? 'addLayer' : 'removeLayer'](this.groups.photoExifs);
    }
    getAreaMarker(a){
        const am = this.markers.areas;
        if (!a.location) return null;
        am[a.uuid] = am[a.uuid] || common.markers.area(a.location, a);
        return am[a.uuid];
    }
    addAreaMarker(a){
        this.getAreaMarker(a)?.addTo(this.groups.areas);
    }
    setAreaVisibility(show){
        this.map[show ? 'addLayer' : 'removeLayer'](this.groups.areas);
    }
    getApproachMarker(a){
        const am = this.markers.approaches;
        if (!a.path) return null;
        am[a.uuid] = am[a.uuid] || common.markers.approach(a.path, a);
        return am[a.uuid];
    }
    addApproachMarker(a){
        this.getApproachMarker(a)?.addTo(this.groups.approaches);
    }
    setApproachVisibility(show){
        this.map[show ? 'addLayer' : 'removeLayer'](this.groups.approaches);
    }
    getParkingMarker(p){
        if (!p.location) return null;
        const pm = this.markers.parkings;
        pm[p.uuid] = pm[p.uuid] || common.markers.parking(p.location, p);
        return pm[p.uuid];
    }
    addParkingMarker(p){
        this.getParkingMarker(p)?.addTo(this.groups.parkings);
    }
    setParkingVisibility(show){
        if (show) this.map.addLayer(this.groups.parkings);
        else      this.map.removeLayer(this.groups.parkings);
    }
    getMap(){ return this.map; }
    getMapCenter(){
        return this.map.getCenter();
    }
    getMapBounds(){
        return this.map.getBounds();
    }
}

class VM {
    constructor(){
        let self = this;
        this.showClimbs = true;
        this.showAreas = true;
        this.showPhotos = true;
        this.showPhotoExifs = true;
        this.showApproaches = true;
        this.showParkings = true;
        this.photoExif = {};
        Promise.all([
            m.request('/climbs'),
            m.request('/areas'),
            m.request('/parkings'),
            m.request('/photos'),
            m.request('/approaches'),
        ]).then(function(res){
            let climbs     = self.climbs     = res[0];
            let areas      = self.areas      = res[1];
            let parkings   = self.parkings   = res[2];
            let photos     = self.photos     = res[3];
            let approaches = self.approaches = res[4];
            self.perim = util.bbox([
                climbs.filter(c => c.location).map(climbCoords),
                areas.filter(c => c.location).map(areaCoords),
                parkings.filter(c => c.location).map(parkingCoords),
                photos.filter(c => c.realLocation).map(photoCoords),
                approaches.filter(a => a.path).map(approachCoords),
            ]);
            Promise.all(photos.filter(p => !p.realLocation).map((p) => {
                return m.request('/photos/' + p.uuid + '/exif').then(data => {
                    self.photoExif[p.uuid] = data;
                });
            })).then(_ => m.redraw());
            self.loaded = true;
            function areaCoords(a){
                if (!a.location) {
                    return null;
                } else {
                    if (a.location.type === "perimeter") {
                        return a.location.polygon.map(pt => [pt.longitude, pt.latitude])
                    } else if (a.location.type === "approximation") {
                        // TODO: do a real calculation for area stuff
                        return [a.location.circle.longitude, a.location.circle.latitude]
                    } else {
                        throw Error('this should never happen')
                    }
                }
            }
            function photoCoords(a){
                if (a.realLocation) {
                    return [a.realLocation.longitude, a.realLocation.latitude];
                } else {
                    return null;
                }
            }
            function climbCoords(a){
                return a.location ? [a.location.longitude, a.location.latitude] : null;
            }
            function parkingCoords(a){
                return a.location ? [a.location.longitude, a.location.latitude] : null;
            }
            function approachCoords(a){
                return a.path ? a.path.map(pt => [pt.longitude, pt.latitude]) : null;
            }
        });
    }
    geohackUrl(){
        let lat = 0;                                
        let lng = 0;                                
        return `https://geohack.toolforge.org/geohack.php?params=${lat};${lng}`;
    }
}

function updateMap(vm, vnode){
    if (!vm.loaded) return;
    const s = vnode.state;
    const pool = vnode.state.pool;
    const m = vnode.state.pool.getMap();
    if (!s.initialCenterOnArea) {
        m.fitBounds([
            L.latLng(vm.perim.ymin, vm.perim.xmin),
            L.latLng(vm.perim.ymax, vm.perim.xmax),
        ]);
        s.initialCenterOnArea = true;
    }
    vm.climbs.filter(c => c.location).forEach(c => pool.addClimbMarker(c));
    pool.setClimbVisibility(vm.showClimbs);
    vm.areas.filter(a => a.location).forEach(a => pool.addAreaMarker(a));
    pool.setAreaVisibility(vm.showAreas);
    vm.photos.filter(p => p.realLocation).forEach(p => pool.addPhotoMarker(p));
    pool.setPhotoVisibility(vm.showPhotos);
    vm.approaches.filter(a => a.path).forEach(a => pool.addApproachMarker(a));
    pool.setApproachVisibility(vm.showApproaches);
    vm.parkings.filter(p => p.location).forEach(p => pool.addParkingMarker(p));
    pool.setParkingVisibility(vm.showParkings);
    if (Object.getOwnPropertyNames(vm.photoExif).length) {
        vm.photos.filter(p => !p.location).forEach(p => {
            const pt = vm.photoExif[p.uuid]?.location;
            pool.addPhotoExifMarker(pt, p);
        })
        pool.setPhotoExifVisibility(vm.showPhotoExifs);
    } else {
        pool.setPhotoExifVisibility(false);
    }
}

window.pages.Map = {
    oninit: function(){
        this.vm = new VM();
        document.title = 'Map';
    },
    view: function(vnode) {
        const vm = this.vm;
        return m('article.map-page', [
            m('.map-page__map', {
                oncreate: function(vnode){
                    const s = vnode.state;
                    this.initialCenterOnArea = false;
                    this.pool = new MapPool(vnode.dom);
                    this.pool.getMap().on('zoomend moveend', (e) => {
                        console.log('TODO: set bounds (and center) of map', e.target.getBounds());
                        // m.redraw();
                    });
                    updateMap(vm, vnode);
                },
                onupdate: function(vnode){ updateMap(vm, vnode); },
            }),
            m('.map-page__controls', [
                m('label', m('input', {
                    type: 'checkbox',
                    value: 'showClimbs',
                    checked: vm.showClimbs,
                    oninput: e => vm.showClimbs = !!e.target.checked,
                }), 'climbs'),
                m('label', m('input', {
                    type: 'checkbox',
                    value: 'showAreas',
                    checked: vm.showAreas,
                    oninput: e => vm.showAreas = !!e.target.checked,
                }), 'areas'),
                m('label', m('input', {
                    type: 'checkbox',
                    value: 'showPhotos',
                    checked: vm.showPhotos,
                    oninput: e => vm.showPhotos = !!e.target.checked,
                }), 'photos with location'),
                m('label', m('input', {
                    type: 'checkbox',
                    value: 'showPhotoExifs',
                    checked: vm.showPhotoExifs,
                    oninput: e => vm.showPhotoExifs = !!e.target.checked,
                }), 'photos with only exif gps'),
                m('label', m('input', {
                    type: 'checkbox',
                    value: 'showApproaches',
                    checked: vm.showApproaches,
                    oninput: e => vm.showApproaches = !!e.target.checked,
                }), 'approaches'),
                m('label', m('input', {
                    type: 'checkbox',
                    value: 'showParkings',
                    checked: vm.showParkings,
                    oninput: e => vm.showParkings = !!e.target.checked
                }), 'parking areas'),
            ]),
            m('.map-page__supplement', [
                m('h2', 'supplement'),
                m('p', [
//                    `items nearest the point (lng:${vm.getFocusCenter()?.lng}, lat:${vm.getFocusCenter()?.lat})`,
                ]),
                m('a', {
                    href: vm.geohackUrl(),
                    disabled: 'disabled',
                }, 'geohack'),
            ])
        ]);
    },
};
})(window, document);
