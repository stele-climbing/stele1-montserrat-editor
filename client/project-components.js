window.components.project = {};

window.components.project.UUID = {
    view: function(vnode){
        return m('input', {
            value: vnode.attrs.value,
            placeholder: 'uuid',
            disabled: true,
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.project.Name = {
    view: function(vnode){
        return m('input', {
            value: vnode.attrs.value,
            placeholder: 'name',
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.project.Description = {
    view: function(vnode){
        return m('textarea', {
            value: vnode.attrs.value,
            placeholder: 'description',
            rows: 2,
            cols: 50,
            oninput: e => vnode.attrs.onchange(e.target.value.trim() !== "" ? e.target.value : null),
        });
    },
};

window.components.project.Authors = {
    view: function(vnode){
        let authors = vnode.attrs.value ? vnode.attrs.value.slice() : [];
        return m('div', authors.map(function(author, idx){
            return m('div', [
                m('input', {
                    value: author,
                    size: 50,
                    placeholder: 'author',
                    oninput: function(e){
                        authors[idx] = e.target.value;
                        vnode.attrs.onchange(authors);
                    },
                }),
                m('button', {
                    onclick: function(){
                        vnode.attrs.onchange(authors.slice(0,idx).concat(authors.slice(idx+1)))
                    },
                }, 'remove')
            ]);
        }).concat([m('div', [
            m('input', {
                placeholder: 'new author',
                value: '',
                size: 50,
                oninput: function(e){
                    if (e.target.value != '') {
                        vnode.attrs.onchange(authors.concat([e.target.value]))
                    }
                },
            }),
        ])]))
    },
};
