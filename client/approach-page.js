pages.Approach = {
    oninit: function(vnode){
        this.approach = null;
        m.request('/approaches/' + vnode.attrs.id).then(a => this.approach = a);
    },
    save: util.debounce(1000, function(){
        Object.keys(this.approach).forEach(k => {
            if (this.approach[k] === null) delete this.approach[k];
        });
        m.request({
            method: 'PUT',
            url: '/approaches/:id',
            params: {id: this.approach.uuid},
            body: this.approach,
        });
    }),
    view: function(vnode) {
        document.title = this.approach?.name || '[APPROACH UNNAMED]';
        return !this.approach ? m('p', 'loading') : m('article.standard.approach-page', [
            m('h1', 'edit approach'),
            m('.name', m(components.approach.Name, {
              value: this.approach.name,
              onchange: n => { this.approach.name = n; this.save() },
            })),
            m('.uuid', m(components.approach.UUID, {
              value: this.approach.uuid
            })),
            m('.notes', m(components.approach.Description, {
              value: this.approach.description,
              onchange: v => { this.approach.description = v; this.save() },
            })),
            m('.path', m(components.approach.Path, {
              value: this.approach.path,
              onchange: v => { this.approach.path = v; this.save() },
            })),
            m('.summary', {
                style: {border: 'solid 1px black', 'white-space': 'pre'}
            }, JSON.stringify(this.approach, null, 2)),
            m('button', {
              style: {color: 'white', background: 'red'},
              onclick: () => {
                m.request({
                  url: '/approaches/' + this.approach.uuid,
                  method: 'DELETE',
                }).then(() => m.route.set('/approaches'));
              }
            }, 'delete approach'),
        ]);
    },
};

