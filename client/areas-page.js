pages.Areas = {
    oninit: function(){
        document.title = 'Areas';
        this.areas = null;
        this.newAreaName = '';
        m.request('/areas').then(a => this.areas = a);
    },
    view: function() {
        return m('article.standard', [
            m('h1', 'areas'),
            m('section', [
              m('input', {
                type: 'text',
                value: this.newAreaName,
                placeholder: 'new area name',
                oninput: e => this.newAreaName = e.target.value,
              }),
              m('button', {
                disabled: !this.newAreaName.trim(),
                onclick: e => {
                  m.request({
                    method: 'POST',
                    url: '/areas',
                    body: {name: this.newAreaName}
                  }).then(_ => {
                    m.request('/areas').then(a => this.areas = a);
                  });
                }
              }, 'add area'),
            ]),
            m('ul', (this.areas || []).map(function(a){ 
                return m('li', m(m.route.Link, {href: '/areas/' + a.uuid}, a.name));
            })),
        ]);
    },
};
