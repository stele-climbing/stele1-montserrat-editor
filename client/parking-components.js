window.components.parking = {};

window.components.parking.Name = {
    view: function(vnode){
        return m('input.parking-name', {
            value: vnode.attrs.value,
            placeholder: 'parking name',
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.parking.Description = {
    view: function(vnode){
        return m('textarea.parking-name', {
            value: vnode.attrs.value,
            rows: 3,
            cols: 50,
            placeholder: 'parking description',
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.parking.Capacity = {
    view: function(vnode){
        return m('input.parking-name', {
            value: vnode.attrs.value,
            type: 'number',
            placeholder: 'capacity',
            oninput: e => {
                let v = e.target.value;
                vnode.attrs.onchange(util.looksLikeNumber(v) ? Number(v) : v);
            },
        });
    },
};

window.components.parking.UUID = {
    view: function(vnode){
        return m('input', {
            value: vnode.attrs.value,
            placeholder: 'uuid',
            disabled: true,
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.parking.Location = {
    view: function(vnode){
        return m('.parking-location', {style: {height: '400px'}}, [
           m(common.PointSetter, {
               value: vnode.attrs.value,
               oninput: pt => {
                   vnode.attrs.onchange(pt);
               }
           }),
        ]);
    },
};
