
class ClimbManagerViewModel {
    constructor(){
        this.climbs = [];
        this.photos = [];
        m.request('/climbs').then(a => this.climbs = a);
        m.request('/photos').then(a => this.photos = a);
    }
    getClimbs(){
        return this.climbs.sort((a, b) => a.name.localeCompare(b.name));
    }
    notDrawnOnPhotos(){
        if (!this.photos.length) return [];
        let drawn = new Set();
        for (let p of this.photos) {
            if (p.climbLayers?.length) {
                for (let cl of p.climbLayers) drawn.add(cl.climbUuid)
            }
        }
        return this.climbs.filter(c => !drawn.has(c.uuid));
    }
    potentialDuplicates(){
        let n = {};
        for (let c of this.climbs) {
            let lcn = c.name.toLocaleLowerCase();
            if (n.hasOwnProperty(lcn))
                n[lcn].push(c);
            else
                n[lcn] = [c];
        }
        return Object.values(n).filter(n => n.length > 1)
    }
    groupedByTag(){
        let tags = {};
        for (let c of this.climbs) {
            for (let t of (c.tags || [])) {
                if (!tags[t]) tags[t] = [];
                tags[t].push(c);
            }
        }
        return Object.entries(tags).sort((a, b) => a[0].localeCompare(b[0]));
    }
    groupedByField(){
        let fields = {};
        for (let c of this.climbs) {
            for (let f in (c.fields || {})) {
                if (!fields[f]) fields[f] = [];
                fields[f].push(c);
            }
        }
        return Object.entries(fields).sort((a, b) => a[0].localeCompare(b[0]));
    }
    groupedByRatingDifficulty(){
        let difficulty = {};
        let ungraded = [];
        for (let c of this.climbs) {
            if (c?.rating?.difficulty) {
                let d = c.rating.difficulty;
                if (difficulty.hasOwnProperty(d))
                    difficulty[d].push(c);
                else
                    difficulty[d] = [c];
            } else {
                ungraded.push(c);
            }
        }
        difficulty['[UNGRADED]'] = ungraded;
        return Object.entries(difficulty).sort(([a, ac], [b, bc]) => a.localeCompare(b));
    }
}

pages.ClimbManager = {
    oninit: function(){
        this.vm = new ClimbManagerViewModel();
        document.title = 'Climb Manager';
    },
    view: function() {
        const vm = this.vm;
        return m('article.climb-manager', [
            m('h1', 'Climb Manager'),
            m('article.climb-manager-duplicates', [
                m('h2', 'Potential Duplicates'),
                m('ul', vm.potentialDuplicates().map(climbs => {
                  return m('li', climbs.map(c => m('span', m(m.route.Link, {
                    href: `/climbs/${c.uuid}`
                  }, c.name))));
                })),
                vm.potentialDuplicates().length ? null : m('em', 'no duplicates found'),
            ]),
            m('article.climb-manager-undrawn', [
                m('h2', 'Not Drawn on Photos'),
                m('ul.plain-climb-list', vm.notDrawnOnPhotos().map(c => m('li', m(m.route.Link, {
                     href: `/climbs/${c.uuid}`
                }, c.name)))),
                vm.notDrawnOnPhotos().length ? null : m('em', 'all climbs are drawn on photos'),
            ]),
            m('article.climb-manager-tags', [
                m('h2', 'Tags'),
                vm.groupedByTag().map(([tag, climbs]) => {
                  return m('details', [
                    m('summary.climb-manager-tag-name', tag),
                    m('ul.plain-climb-list', climbs.map(c => m('li', m(m.route.Link, {
                      href: `/climbs/${c.uuid}`
                    }, c.name))))
                  ]);
                }),
                vm.groupedByTag().length ? null : m('em', 'no tags in use'),
            ]),
            m('article.climb-manager-fields', [
                m('h2', 'Fields'),
                m('div', vm.groupedByField().map(([field, climbs]) => {
                  return m('details', [
                    m('summary.climb-manager-field-name', field),
                    m('ul.plain-climb-list', climbs.map(c => m('li', m(m.route.Link, {
                      href: `/climbs/${c.uuid}`
                    }, c.name))))
                  ]);
                })),
                vm.groupedByField().length ? null : m('em', 'no fields in use'),
            ]),
            m('article.climb-manager-difficulty', [
                m('h2', 'By Difficulty'),
                m('.climb-manager-difficulty__grades', vm.groupedByRatingDifficulty().map(([g, climbs]) => {
                    return m('details', [
                        m('summary.climb-manager-difficulty__grade.climb-grade-difficulty', g),
                        m('ul.climb-manager-difficulty__grade-climbs', climbs.map(c => m('li', m(m.route.Link, {
                            href: `/climbs/${c.uuid}`
                        }, c.name))))
                    ]);
                }))
            ]),
            m('article.climb-manager__ungraded-projects', [
                m('h2', 'Ungraded Projects'),
                m('p', [
                    'projects are explicitly marked, so adding a grade can help'
                ]),
                m('ul.plain-climb-list', vm.getClimbs().filter(c => {
                    return c.projectStatus && !c?.rating?.difficulty;
                }).map(function(c){ 
                    return m('li', [
                        m(m.route.Link, {href: '/climbs/' + c.uuid}, c.name)
                    ]);
                })),
            ]),
        ]);
    },
};
