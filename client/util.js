(function(undefined){

let debounce = function(time, fn){
    let to = null;
    return function(){
        let args = Array.prototype.slice.call(arguments);
        let self = this;
        if (to) clearTimeout(to);
        to = setTimeout(function(){
            fn.apply(self, args);
        }, time);
        return null;
    };
};

let copy = function(a){
    let s = JSON.stringify(a);
    let b = (s !== undefined) ? JSON.parse(s) : s;
    return b;
};

let looksLikeNumber = function(a){
    return !!a.trim().match(/^-?[0-9]+(\.[0-9]+)?$/);
};

let looksLikeInt = function(a){
    return a.trim().match(/^-?[0-9]+$/);
};

let reverse = function(a){
    return a.slice().reverse();
};

let imageDimensions = function(url){
    var p = new Promise(function(resolve, reject){
        var img = new Image();
        img.onload = function(e){
            resolve({
                width: img.naturalWidth,
                height: img.naturalHeight,
            });
        };
        img.src = url;
    });
    return p;
};

let bbox = function(){
    let empty = function(){ return {xmin: null, ymin: null, xmax: null, ymax: null} };
    let box = empty();
    let extend = function(a, x, y){
        return {
            xmin: a.xmin === null ? x : Math.min(a.xmin, x),
            xmax: a.xmax === null ? x : Math.max(a.xmax, x),
            ymin: a.ymin === null ? y : Math.min(a.ymin, y),
            ymax: a.ymax === null ? y : Math.max(a.ymax, y),
        };
    };
    function aux(a, v){
        if (!Array.isArray(v)) throw Error('bbox can only handle nested arrays with numbers')
        if (!v.length) {
            return a;
        } else if (typeof v[0] == 'number') {
            return extend(a, v[0], v[1]);
        } else if (Array.isArray(v[0])) {
            return aux(aux(a, v[0]), v.slice(1));
        }
    };
    for (let arg of arguments) {
        box = aux(box, arg);
    }
    return (box.xmin !== null) ? box : null;
};

let roughBounds = (function(){
    let cache = null;
    function areaCoords(a){
        if (!a.location) {
            return null;
        } else {
            if (a.location.type === "perimeter") {
                return a.location.polygon.map(pt => [pt.longitude, pt.latitude])
            } else if (a.location.type === "approximation") {
                // TODO: do a real calculation for area stuff
                return [a.location.circle.longitude, a.location.circle.latitude]
            } else {
                throw Error('this should never happen')
            }
        }
    }
    function photoCoords(a){ //TODO: support location from exif data
        if (a.realLocation) {
            return [a.realLocation.longitude, a.realLocation.latitude];
        } else {
            return null; 
        }
    }
    function climbCoords(a){
        return a.location ? [a.location.longitude, a.location.latitude] : null;
    }
    function parkingCoords(a){
        return a.location ? [a.location.longitude, a.location.latitude] : null;
    }
    function approachCoords(a){
        return a.path ? a.path.map(pt => [pt.longitude, pt.latitude]) : null;
    }
    function refresh(){
        let self = this;
        Promise.all([
            m.request('/climbs'),
            m.request('/areas'),
            m.request('/parkings'),
            m.request('/photos'),
            m.request('/approaches'),
        ]).then(function(res){
            let climbs     = self.climbs     = res[0];
            let areas      = self.areas      = res[1];
            let parkings   = self.parkings   = res[2];
            let photos     = self.photos     = res[3];
            let approaches = self.approaches = res[4];
            cache = util.bbox([
                climbs.filter(c => c.location).map(climbCoords),
                areas.filter(c => c.location).map(areaCoords),
                parkings.filter(c => c.location).map(parkingCoords),
                photos.filter(c => c.realLocation).map(photoCoords),
                approaches.filter(a => a.path).map(approachCoords),
            ]);
        });
    };
    function getter(){
        return copy(cache);
    };
    function getAsLeafletBounds(){
        let bbox = getter();
        if (!bbox) return null;
        let topLeft = L.latLng(bbox.ymax, bbox.xmin)
        let bottomRight = L.latLng(bbox.ymin, bbox.xmax)
        return L.latLngBounds(topLeft, bottomRight);
    };
    getter.refresh = refresh;
    getter.getAsLeafletBounds = getAsLeafletBounds;
    return getter;
})();

const serial = (function(){
   let next = 0;
   return function(){
       next += 1;
       return next;
   }
}());

const stamp = function(v){
    if (typeof v !== 'object') return null;
    if (!v.hasOwnProperty('__stamp'))
        v.__stamp = serial();
    return v.__stamp
};

window.util = {
    stamp,
    serial,
    bbox,
    debounce,
    copy,
    reverse,
    roughBounds,
    looksLikeNumber,
    looksLikeInt,
    imageDimensions
};

}())

