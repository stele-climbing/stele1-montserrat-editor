(function(window, document){

class VM {
    addClimb(c){ this._climbs.push(c); }
    mapClimbs(f){
        return this._climbs.map(c => ({
            item: c,
            missingLocation: !c.location,
        })).filter(a => a.missingLocation).map(f);
    }
    addPhoto(p){ this._photos.push(p); }
    mapPhotos(f){
        return this._photos.map(p => ({
            item: p,
            missingLocation: !p.realLocation,
        })).filter(a => a.missingLocation).map(f);
    }
    constructor(){
        this._climbs = [];
        this._photos = [];
        let self = this;
        Promise.all([
            m.request('/climbs'),
            m.request('/areas'),
            m.request('/parkings'),
            m.request('/photos'),
            m.request('/approaches'),
        ]).then((res) => {
            for (let climb of res[0]) this.addClimb(climb);
            for (let photo of res[3]) this.addPhoto(photo);
            // let areas      = self.areas      = res[1];
            // let parkings   = self.parkings   = res[2];
            // let approaches = self.approaches = res[4];
        });
    }
}


window.pages.Urgent = {
    oninit: function(){
        this.vm = new VM();
        document.title = 'Urgent';
    },
    view: function() {
        let vm = this.vm;
        return m('article.urgent-page', [
            m('h1', 'Urgent'),
            m('section.missing-locations', [
                m('h2', 'Missing Locations'),
                m('ul', [
                    m('li', [
                        'areas missing a location',
                        m('ul', (vm.areas || []).filter(a => !a.location).map(function(a){
                            return m('li', m(m.route.Link, {href: '/areas/' + a.uuid}, a.name));
                        }))
                    ]),
                    m('li', [
                        'parkings missing a location',
                        m('ul', (vm.parkings || []).filter(p => !p.location).map(function(p){
                            return m('li', m(m.route.Link, {href: '/parkings/' + p.uuid}, p.uuid));
                        }))
                    ]),
                ]),
            ]),
            m('section.base-requirements', [
                m('header', [
                    m('h2', 'Base Requirements'),
                    m('p', [
                       'Some info is nice to have. ',
                       'Each items below is missing data that\'s core to its identity. '
                    ]),
                ]),
                m('table.base-reqs-table', [
                  m('thead', [
                    m('tr', [
                      m('th', 'item'),
                      m('th', 'kind'),
                      m('th', 'missing location'),
                      // m('th', 'base facts (name, location, grade/project/ascent)'),
                    ])
                  ]),
                  m('tbody', [
                    vm.mapClimbs(e => m('tr', [
                      m('td', m(m.route.Link, {href: '/climbs/' + e.item.uuid}, e.item.name)),
                      m('td', 'climb'),
                      m('td', e.missingLocation ? 'missing location' : ''),
                    ])),
                    vm.mapPhotos(e => m('tr', [
                      m('td', m(m.route.Link, {href: '/photos/' + e.item.uuid}, [
                        m('img', {
                            src: '/photos/'+e.item.uuid+'.jpg?covering_square=200',
                            alt: e.item.uuid,
                        }),
                      ])),
                      m('td', 'photo'),
                      m('td', e.missingLocation ? 'missing location' : ''),
                    ])),
                  ]),
                ]),
            ]),
        ]);
    },
};
})(window, document);
