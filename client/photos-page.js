(function(window){

function fileAsBase64(f){
    return new Promise(function(resolve, reject){
        var r = new FileReader();
        r.readAsDataURL(f);
        r.onload = function(){ resolve(r.result.replace(/^.*,/, '')); };
        r.onerror = function(error){ reject(error); };
    });
}

class PhotoUploadItem {
    constructor(f){
        this.file = f;
        this.started = false;
        this.finished = false;
        this.progress = 0;
        this.uploadItemId = util.serial();
        this.xhr = new XMLHttpRequest();
        this.base64 = fileAsBase64(f);
    }
    start(){
        let xhr = this.xhr;
        this.started = true;
        xhr.open('POST', '/photos');
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.upload.addEventListener('progress', e => {
            this.progress = e.loaded / e.total;
            m.redraw();
        });
        return new Promise((resolve, reject) => {
            xhr.addEventListener('load', e => {
                m.redraw();
                this.progress = 1;
                this.finished = true;
                resolve();
            });
            this.base64.then(b64 => {
                xhr.send(JSON.stringify({
                    filename: this.file.name,
                    base64: b64
                }));
            });
        });
    }
    getSummary(){
        return {
            started: this.started,
            finished: this.finished,
            progress: this.progress,
        };
    }
}

class PhotoUploader {
    constructor(){
        this.parallel = 4;
        this.items = [];
        return this;
    }
    update(){
        const inProgress = this.items.filter(i => i.started && !i.finished).length;
        if (inProgress < this.parallel) {
            for (let i of this.items) {
                if (!i.started) {
                    i.start().then(e => this.update());
                    return;
                }
            }
        }
    }
    addFile(f){
        this.items.push(new PhotoUploadItem(f));
        this.update();
    }
    getItems(){
        return [].concat(this.items);
    }
}

function save(payload){
    return m.request({
        method: 'POST',
        url: '/photos',
        body: payload,
    });
}

pages.Photos = {
    oninit: function(){
        document.title = 'Photos';
        this.photos = null;
        this.uploader = new PhotoUploader();
        m.request('/photos').then(a => this.photos = a);
    },
    view: function() {
        console.log(this.uploader)
        return m('article.photos-page', [
            m('header.photos-page__header', [
                m('h1', 'photos'),
                m('details.photo-upload-info', [
                    m('summary', 'upload photo'),
                    m('p', {style: {color: 'red'}}, [
                      'Some devices, notably ',
                      m('a', {
                          // https://stackoverflow.com/questions/16297730/image-upload-from-iphone-strips-exif-data
                          href: 'https://stackoverflow.com/q/16297730'
                      }, 'iPhones'),
                      ', ',
                      'strip GPS info by default. ',
                      'This is often done for privacy, but probably isn\'t what you want in this situation.'
                    ]),
                    m('label', [
                        m('span', 'add photo(s) '),
                        m('input', {
                            type: 'file',
                            multiple: true,
                            onchange: e => {
                                for(let f of e.target.files) this.uploader.addFile(f);
                            },
                        }),
                    ]),
                    m('ul', this.uploader.getItems().map(f => m('li', [
                        f.progress,
                        ' -- ',
                        f.file.name
                    ]))),
                ]),
            ]),
            m('ul.photos-page__photos', (this.photos || []).map(function(p){ 
              return m('li.photos-page__photo', [
                  m(m.route.Link, {href: `/photos/${p.uuid}`}, [
                    m(components.photo.Thumbnail, {
                        photo: p,
                        size: 250,
                    }),
                  ]),
              ]);
            })),
        ]);
    },
};

})(window);
