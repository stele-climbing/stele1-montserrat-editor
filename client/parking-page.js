pages.Parking = {
    oninit: function(vnode){
        this.parking = null;
        m.request('/parkings/' + vnode.attrs.id).then(a => this.parking = a);
    },
    save: util.debounce(1000, function(){
        Object.keys(this.parking).forEach(k => {
            if (this.parking[k] === null) delete this.parking[k];
        });
        m.request({
            method: 'PUT',
            url: '/parkings/:id',
            params: {id: this.parking.uuid},
            body: this.parking,
        });
    }),
    view: function(vnode) {
        document.title = 'Ⓟ ' + (this.parking?.name || '[PARKING UNNAMED]');
        return !this.parking ? m('p', 'loading') : m('article.standard.parking-page', [
            m('h1', 'edit parking'),
            m('.name', m(components.parking.Name, {
              value: this.parking.name,
              onchange: n => { this.parking.name = n; this.save() },
            })),
            m('.uuid', m(components.parking.UUID, {
              value: this.parking.uuid
            })),
            m('.fields', m(components.parking.Capacity, {
              value: this.parking.capacity,
              onchange: a => { this.parking.capacity = a; this.save() },
            })),
            m('.notes', m(components.parking.Description, {
              value: this.parking.description,
              onchange: v => { this.parking.description = v; this.save() },
            })),
            m('.location', m(components.parking.Location, {
              value: this.parking.location,
              onchange: v => { this.parking.location = v; this.save() },
            })),
            m('.summary', {
                style: {border: 'solid 1px black', 'white-space': 'pre'}
            }, JSON.stringify(this.parking, null, 2)),
            m('button', {
              style: {color: 'white', background: 'red'},
              onclick: () => {
                m.request({
                  url: '/parkings/' + this.parking.uuid,
                  method: 'DELETE',
                }).then(() => m.route.set('/parkings'));
              }
            }, 'delete parking'),
        ]);
    },
};

