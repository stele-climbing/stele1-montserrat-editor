pages.Approaches = {
    oninit: function(){
        document.title = 'Approaches';
        this.approaches = null;
        this.newApproachName = '';
        m.request('/approaches').then(a => this.approaches = a);
    },
    view: function() {
        return m('article.standard', [
            m('h1', 'approaches'),
            m('section', [
              m('input', {
                type: 'text',
                value: this.newApproachName,
                placeholder: 'new approach name',
                oninput: e => this.newApproachName = e.target.value,
              }),
              m('button', {
                disabled: !this.newApproachName.trim(),
                onclick: e => {
                  m.request({
                    method: 'POST',
                    url: '/approaches',
                    body: {name: this.newApproachName}
                  }).then(_ => {
                    m.request('/approaches').then(a => this.approaches = a);
                  });
                }
              }, 'add approach'),
            ]),
            m('ul', (this.approaches || []).map(function(a){ 
                return m('li', m(m.route.Link, {href: '/approaches/' + a.uuid}, a.name));
            })),
        ]);
    },
};
