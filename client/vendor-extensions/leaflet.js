(function(){

L.prebuilt = {};
L.prebuilt.layers = {};
L.prebuilt.maps = {};

let layers = L.prebuilt.layers;
let maps = L.prebuilt.maps;

/**
 * other options for topos:
 * http://www.mytopo.com/maps/index.cfm // looks like they switched to google maps
 * http://tileserver.trimbleoutdoors.com/SecureTile/TileHandler.ashx?mapType=Topo&x=1437&y=3302&z=13
**/

L.prebuilt.layers.satellite = function(){
    var url = "https://server.arcgisonline.com/" +
              "ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}";
    var cfg = {
        attribution: "Tiles © Esri — Source: Esri, i-cubed, USDA, USGS, " +
                     "AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, " +
                     "and the GIS User Community"
    };
    return L.tileLayer(url, cfg);
};

L.prebuilt.layers.hereHybrid = function(){
    var url = "https://{s}.{base}.maps.api.here.com/maptile/2.1/maptile/newest/hybrid.day/{z}/{x}/{y}/256/png8" +
              "?app_id={app_id}" +
              "&app_code={app_code}";
    var cfg = {
        attribution: "Map © 1987-2014 <a href='http:developer.here.com'>HERE</a>",
        subdomains: "1234",
        app_id: keys.HERE_APP_ID,
        app_code: keys.HERE_APP_CODE,
        base: "aerial",
        maxZoom: 20,
        type: "maptile",
        language: "eng",
    }
    return L.tileLayer(url, cfg);
};

L.prebuilt.layers.hereSatellite = function(){
    var url = "https://{s}.{base}.maps.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8" +
              "?app_id={app_id}" +
              "&app_code={app_code}";
    var cfg = {
        attribution: "Map © 1987-2014 <a href='http:developer.here.com'>HERE</a>",
        subdomains: "1234",
        app_id: keys.HERE_APP_ID,
        app_code: keys.HERE_APP_CODE,
        base: "aerial",
        maxZoom: 20,
        type: "maptile",
        language: "eng",
    }
    return L.tileLayer(url, cfg);
};



L.prebuilt.layers.topo = function(){
    var url = "https://server.arcgisonline.com" +
              "/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}";
    var cfg = {
        attribution: "Tiles © Esri — Esri, DeLorme, NAVTEQ, TomTom, Intermap, " +
                     "iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, " +
                     "Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community"
    };
    return L.tileLayer(url, cfg);
};

L.prebuilt.layers.streets = function(){
    var url = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
    var cfg = {
        maxZoom: 19,
        attribution: "© <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>"
    }
    return L.tileLayer(url, cfg);
};


L.prebuilt.layers.trails = function(){
    // originally: http://{s}.tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png
    // invalid:    https://{s}.tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png
    // hacky?:     https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png     //hacky?
    var url = "https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png";
    var cfg = {
        maxZoom: 19,
        attribution: "© <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>"
    }
    return L.tileLayer(url, cfg);
};


L.prebuilt.layers.bing = function(){ // from leaflet-bing-layer@3.3.0
    return L.tileLayer.bing({
        bingMapsKey: keys.BING_MAPS_KEY,
        imagerySet: "Aerial",
        maxZoom: 21,
    });
};

L.prebuilt.maps.empty = function(container){
    var map = L.map(container, {
        attributionControl: false,
        zoomControl: false,
    }).defaultView();
    return map;
}

/* TODO: delete me once maps exist
L.prebuilt.maps.plain = function(container){
    var map = L.map(container, {
        attributionControl: false,
        zoomControl: false,
    }).defaultView();
    L.layers.trails().addTo(map);
    return map;
};
*/

L.prebuilt.maps.layered = function(container){
    var map = L.map(container, {
        attributionControl: false,
        zoomControl: false,
    }).defaultView();
    L.layers.trails().addTo(map);
    return map;
};

L.Map.include({
    defaultView: function(){
        var a = L.latLngBounds(L.latLng(65, 68.5), L.latLng(-47, -128));
        return this.fitBounds(a);
    },
})

L.LayerGroup.include({
    isEmpty: function(){
        var isEmpty = true
        this.eachLayer(function(){ isEmpty = false });
        return isEmpty;
    },
    firstLayer: function(){
        var id;
        for (id in this._layers) {
            return this._layers[id];
        }
        return null;
    },
    onlyLayers: function(layers){
        this.eachLayer(function(l){
            if (layers.indexOf(l) >= 0)
                this.removeLayer(l);
        }, this);
        layers.forEach(function(l){
            this.addLayer(l);
        }.bind(this))
    },
});

// HACK: https://github.com/Leaflet/Leaflet.draw/issues/804
L.Polygon.mergeOptions({editing: {}});

})();
