
L.Map
 .mergeOptions({
     mountContainerClass: "leaflet-mount-map-standardtopo",
 })

L.Mount.QuickButton = L.Mount.Leaf.extend({
    initialize: function(getCfg){
        this.getCfg = getCfg;
        L.Mount.Leaf.prototype.initialize.call(this);
    },
    _doAction: function(){
        if (this._tmpAction) {
            this._tmpAction();
        }
        this.updateDom();
        return null;
    },
    acquireElement: function(){
        var btn = L.DomUtil.create("button", "edit-circle");
        console.log('TODO: attach styles: ', btn, '{ pointer-events: all; background-color: white; margin: 5px; cursor: pointer; }');
        L.DomEvent.disableClickPropagation(btn)
        L.DomEvent.on(btn, "click", this._doAction, this);
        return {root: btn}
    },
    updateDom: function(){
        var btn = this.getRootElement();

        var cfg = this.getCfg();

        btn.innerText = cfg.text;

        if (cfg.action) {
            btn.removeAttribute("disabled");
            this._tmpAction = cfg.action;
        } else {
            btn.setAttribute("disabled", "");
            this._tmpAction = null;
        }

        return null;
    },
    linkMap: function(){
        this.updateDom();
    },
});
