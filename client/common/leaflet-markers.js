(function(window, document){

let cm = common.markers = {};

cm.climb = function(location, climb){
    let pt = {lng: location.longitude, lat: location.latitude};
    let centerIcon = L.divIcon({className: 'leaflet-climb-marker--center'});
    let center = L.marker(pt, {icon: centerIcon});
    center.bindTooltip(climb.name);
    let circle = L.circle(pt, {radius: location.meterRadius});
    return {
        addTo: function(target){
           target.addLayer(circle);
           target.addLayer(center);
        }
    };
};

function photoPopupContent(photo){
    let a = document.createElement('a');
    a.href = '#!/photos/' + photo.uuid;
    let img = document.createElement('img');
    img.src = '/photos/' + photo.uuid + '.jpg?covering_square=200';
    a.appendChild(img);
    a.addEventListener('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        m.route.set('/photos/:id', {id: photo.uuid})
        return false;
    });
    return a;
}

cm.photo = function(location, photo){
    let pt = {lng: location.longitude, lat: location.latitude};
    let centerIcon = L.divIcon({
        className: 'leaflet-photo-marker--center',
        iconSize: [8, 8]
    });
    let center = L.marker(pt, {icon: centerIcon});
    center.bindPopup(photoPopupContent(photo));
    let circle = L.circle(pt, {radius: location.meterRadius});
    return {
        addTo: function(target){
           target.addLayer(circle);
           target.addLayer(center);
        }
    };
};


cm.photoExif = function(location, photo){
    let pt = {lng: location.longitude, lat: location.latitude};
    let centerIcon = L.divIcon({
        className: 'leaflet-photo-exif-marker',
        iconSize: [8, 8]
    });
    let center = L.marker(pt, {icon: centerIcon});
    center.bindPopup(photoPopupContent(photo));
    return {
        addTo: function(target){
           target.addLayer(center);
        }
    };
};


cm.approach = function(path, approach){
    let pts = path.map(function(pt){
        return {lng: pt.longitude, lat: pt.latitude};
    });
    var line = L.polyline(pts, {color: 'yellow'});
    return {
        addTo: function(target){
           target.addLayer(line);
        }
    };
};

cm.parking = function(location, photo){
    let icon = L.divIcon({
        className: 'leaflet-parking-marker',
        html: 'P',
        iconAnchor: [10, 10],
        iconSize: [20, 20],
    });
    let marker = L.marker({
        lng: location.longitude,
        lat: location.latitude
    }, {icon: icon});
    return {
        addTo: function(target){
           target.addLayer(marker);
        }
    };
};

cm.area = function(location, area){
    if (location.type === 'approximation')
        return areaApproximation(location.circle, area)
    else if (location.type === 'perimeter')
        return areaPerimeter(location.polygon, area)
}
function areaPerimeter(polygon, area){
    let pts = polygon.map(function(pt){
        return {lng: pt.longitude, lat: pt.latitude};
    });
    var line = L.polygon(pts, {color: '#00FFFF', fill: false});
    line.bindTooltip(area.name);
    return {
        addTo: function(target){
           target.addLayer(line);
        }
    };
};
function areaApproximation(location, area){
    let pt = {lng: location.longitude, lat: location.latitude};
    let centerIcon = L.divIcon({className: 'leaflet-area-marker--approximation-center'});
    let center = L.marker(pt, {icon: centerIcon});
    let circle = L.circle(pt, {
        radius: location.meterRadius,
        color: '#00FFFF',
        fill: false
    });
    circle.bindTooltip(area.name);
    return {
        addTo: function(target){
           target.addLayer(circle);
           // target.addLayer(center);
        }
    };
};

})(window, document)
