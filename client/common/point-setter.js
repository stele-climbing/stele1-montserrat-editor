
common.PointSetter = {
    oncreate: function(vnode){
        var tm = this.map = L.prebuilt.maps.empty(vnode.dom);
        var group = this.group = new L.FeatureGroup();

        var trails = L.prebuilt.layers.trails();
        var streets = L.prebuilt.layers.streets();
        var satellite1 = L.prebuilt.layers.satellite();
        var satellite2 = L.prebuilt.layers.hereSatellite();
        var hybrid = L.prebuilt.layers.hereHybrid();
        var topo = L.prebuilt.layers.topo();
        var bing = L.prebuilt.layers.bing();

        var baseSelect = new L.Mount.BaseLayerSelect();

        tm.mount(baseSelect);

        tm.addLayer(hybrid);

        baseSelect
            .manageLayer({
                title: "bing",
                layer: bing,
            })
            .manageLayer({
                title: "streets",
                layer: streets,
            })
            .manageLayer({
                title: "satellite 1",
                layer: satellite1,
            })
            .manageLayer({
                title: "satellite 2",
                layer: satellite2,
            })
            .manageLayer({
                title: "hybrid",
                layer: hybrid,
            })
            .manageLayer({
                title: "topo",
                layer: topo,
            })
            .manageLayer({
                title: "trails",
                layer: trails,
            });

        this.map.addLayer(group);

        var self = this;

        let doneInitialPositioning = false;

        let marker = this.marker = L.marker([0, 0], {draggable: true});

        this.marker.on('dragend', function(e){
            console.log(e);
            updateLocation();
        });

        if (vnode.attrs.value) {
            this.marker.setLatLng([vnode.attrs.value.latitude, vnode.attrs.value.longitude]);
            this.marker.addTo(tm);
            tm.setView(this.marker.getLatLng(), 13);
            doneInitialPositioning = true;
        }

        if (!doneInitialPositioning) {
            tm.defaultView();
        }

        var btn = this.button = new L.Mount.QuickButton(function(){
            var start = {
                text: "place point",
                action: function(){
                    marker.setLatLng(tm.getCenter());
                    marker.addTo(tm);
                    tm.addLayer(marker);
                    marker.dragging.enable();
                    console.log(tm.getCenter());
                    marker.setLatLng(tm.getCenter());
                    updateLocation();
                }
            };
            var stop = {
                text: "remove circle",
                action: function(){
                    marker.removeFrom(tm);
                    updateLocation();
                },
            };
            return tm.hasLayer(marker) ? stop : start;
        });
        tm.mount(btn);

        function updateLocation(){
            if (tm.hasLayer(marker)) {
                let a = marker.getLatLng();
                vnode.attrs.oninput({longitude: a.lng, latitude: a.lat});
            } else {
                vnode.attrs.oninput(null);
            }
            m.redraw();
        }
    },
    onupdate: function(vnode){
        if (vnode.attrs.value) {
            this.marker.setLatLng([vnode.attrs.value.latitude, vnode.attrs.value.longitude]);
        } else {
            this.marker.removeFrom(this.map);
        }
    },
    onremove: function(vnode){
        this.map.remove()
    },
    view: function(vnode){
        return m("div", Object.assign({style: {height: '400px'}}, vnode.attrs));
    }
};
