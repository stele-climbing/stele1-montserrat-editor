common.PathSetter = {
    oncreate: function(vnode){
        var tm = this.map = L.prebuilt.maps.empty(vnode.dom);
        var group = this.group = new L.FeatureGroup();

        var trails = L.prebuilt.layers.trails();
        var streets = L.prebuilt.layers.streets();
        var satellite1 = L.prebuilt.layers.satellite();
        var satellite2 = L.prebuilt.layers.hereSatellite();
        var hybrid = L.prebuilt.layers.hereHybrid();
        var topo = L.prebuilt.layers.topo();
        var bing = L.prebuilt.layers.bing();

        var baseSelect = new L.Mount.BaseLayerSelect();

        tm.mount(baseSelect);

        tm.addLayer(hybrid);

        baseSelect
            .manageLayer({
                title: "bing",
                layer: bing,
            })
            .manageLayer({
                title: "streets",
                layer: streets,
            })
            .manageLayer({
                title: "satellite 1",
                layer: satellite1,
            })
            .manageLayer({
                title: "satellite 2",
                layer: satellite2,
            })
            .manageLayer({
                title: "hybrid",
                layer: hybrid,
            })
            .manageLayer({
                title: "topo",
                layer: topo,
            })
            .manageLayer({
                title: "trails",
                layer: trails,
            });

        this.map.addLayer(group);

        function setExisting(){
            var p = vnode.attrs.getPath();
            if (!p) return;
            var pgon = L.polyline(L.lnglat.yx(p));
            pgon.editing.enable();
            group.addLayer(pgon);
            /* pgon.on("remove", edit.disable, edit)*/
            tm.fitBounds(group.getBounds());
        }
        setExisting();

        var handler = this.handler = new L.Draw.Polyline(this.map, {});

        var btn = this.button = new L.Mount.QuickButton(function(){
            var start = {
                text: "start drawing",
                action: function(){
                    handler.enable();
                }
            };
            var cancel = {
                text: "cancel drawing",
                action: function(){
                    handler.disable();
                },
            };
            var restart = {
                text: "remove path",
                action: function(){
                    group.eachLayer(function(l){
                        l.editing.disable();
                        group.removeLayer(l);
                    });
                    unsetPath();
                    return null;
                },
            };
            if (group.isEmpty()) {
                if (handler.enabled()) {
                    return cancel;
                } else {
                    return start;
                }
            } else {
                return restart;
            }
        });

        tm.mount(btn);

        var editEvents = [L.Draw.Event.EDITED,
                          L.Draw.Event.EDITRESIZE,
                          L.Draw.Event.EDITVERTEX,
                          L.Draw.Event.EDITMOVE].join(" ")

        var createEvent = L.Draw.Event.CREATED;

        tm.on(createEvent, function(e){
            let layer = e.layer;
            setPath(layer);
            this.group.addLayer(layer);
            layer.editing.enable();
            handler.disable();
            btn.updateDom();
        }, this);

        tm.on(editEvents, function(e){
            setPath(e.poly);
            btn.updateDom();
        }, this);

        function unsetPath(){
            vnode.attrs.setPath(null);
            m.redraw();
        }

        function setPath(layer){
            vnode.attrs.setPath(layer.toGeoJSON().geometry.coordinates);
            m.redraw();
        }
    },
    view: function(){
        return m("div", {
            style: {
                width: "100%",
                height: "100%",
            }
        });
    },
};
