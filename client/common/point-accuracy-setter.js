
common.PointAccuracySetter = {
    oncreate: function(vnode){
        var tm = this.map = L.prebuilt.maps.empty(vnode.dom);
        var group = this.group = new L.FeatureGroup();

        var trails = L.prebuilt.layers.trails();
        var streets = L.prebuilt.layers.streets();
        var satellite1 = L.prebuilt.layers.satellite();
        var satellite2 = L.prebuilt.layers.hereSatellite();
        var hybrid = L.prebuilt.layers.hereHybrid();
        var topo = L.prebuilt.layers.topo();
        var bing = L.prebuilt.layers.bing();

        var baseSelect = new L.Mount.BaseLayerSelect();

        tm.mount(baseSelect);

        tm.addLayer(bing);

        baseSelect
            .manageLayer({
                title: "bing",
                layer: bing,
            })
            .manageLayer({
                title: "streets",
                layer: streets,
            })
            .manageLayer({
                title: "satellite 1",
                layer: satellite1,
            })
            .manageLayer({
                title: "satellite 2",
                layer: satellite2,
            })
            .manageLayer({
                title: "hybrid",
                layer: hybrid,
            })
            .manageLayer({
                title: "topo",
                layer: topo,
            })
            .manageLayer({
                title: "trails",
                layer: trails,
            });

        this.map.addLayer(group);

        var self = this;

        var doneInitialPositioning = false;

        if (vnode.attrs.suggestion) {
            console.error("PointAccuracySetter's suggestion deprecated");
            let sug = vnode.attrs.suggestion;
            var exif = self.exif = L.marker(L.lnglat.yx(sug.point));
            exif.addTo(tm)
            tm.setView(L.lnglat.yx(sug.point), 15);
            doneInitialPositioning = true;
        }

        if (vnode.attrs.approximateLocation) {
            console.error("PointAccuracySetter's approximateLocation deprecated");
            let al = vnode.attrs.approximateLocation;
            var circ = self.circ = common.map.markers.accuracy( // TODO: draw a circle instead
                L.lnglat.yx(al.point), al.accuracy
            );
            circ.setStyle({fill: false});
            circ.addTo(tm)
            circ.bindTooltip('approximate area', {sticky: true});
            tm.fitBounds(circ.getBounds());
            doneInitialPositioning = true;
        }

        if (vnode.attrs.suggestedBounds) {
            tm.fitBounds(vnode.attrs.suggestedBounds);
            doneInitialPositioning = true;
        }

        if (vnode.attrs.suggestedCenter) {
            let c = vnode.attrs.suggestedCenter;
            let pt = {lat: c.point.latitude, lng: c.point.longitude};
            var mk = self.suggestedCenterMarker = L.marker(pt);
            mk.addTo(tm)
            tm.setView(pt, 15);
            if (!vnode.attrs.getCircle())
                mk.bindPopup(vnode.attrs.suggestedCenter.popupContent).openPopup();
            doneInitialPositioning = true;
        }

        if (vnode.attrs.getCircle()) {
            let a = vnode.attrs.getCircle();
            var circle = self.circle = L.circle()
                .setLngLat(a.point || a.center)
                .setRadius(a.accuracy);
            var edit = new L.Edit.Circle(circle);
            group.addLayer(circle);
            edit.enable();
            circle.on("remove", edit.disable, edit)
            tm.fitBounds(group.getBounds());
            doneInitialPositioning = true;
        }

        if (!doneInitialPositioning) {
            tm.defaultView();
        }

        if (vnode.attrs.suggestion && vnode.attrs.suggestion.message) {
            self.exif.bindPopup(vnode.attrs.suggestion.message);
            if (!self.circle) {
                self.exif.openPopup();
            }
        }

        var handler = this.handler = new L.Draw.Circle(this.map, {});

        var btn = this.button = new L.Mount.QuickButton(function(){
            var start = {
                text: "start drawing",
                action: function(){
                    handler.enable();
                }
            };
            var cancel = {
                text: "cancel drawing",
                action: function(){
                    handler.disable();
                },
            };
            var restart = {
                text: "remove circle",
                action: function(){
                    group.eachLayer(function(l){
                        l.editing.disable();
                        group.removeLayer(l);
                    });
                    unsetCircle();
                    return null;
                },
            };
            if (group.isEmpty()) {
                if (handler.enabled()) {
                    return cancel;
                } else {
                    return start;
                }
            } else {
                return restart;
            }
        });

        tm.mount(btn);

        var editEvents = [L.Draw.Event.EDITED,
                          L.Draw.Event.EDITRESIZE,
                          L.Draw.Event.EDITMOVE].join(" ")

        var createEvent = L.Draw.Event.CREATED;

        tm.on(createEvent, function(e){
            let layer = e.layer;
            setCircleAsLayer(layer);
            this.group.addLayer(layer);
            handler.disable();
            layer.editing.enable();
            btn.updateDom();
        }, this);

        tm.on(editEvents, function(e){
            setCircleAsLayer(e.layer);
            btn.updateDom();
        }, this);

        function unsetCircle(){
            vnode.attrs.setCircle(null);
            m.redraw();
        }

        function setCircleAsLayer(layer){
            var xy = L.lnglat.xy(layer.getLatLng());
            vnode.attrs.setCircle({
                accuracy: layer.getRadius(),
                point: xy,
            });
            m.redraw();
        }

    },
    onupdate: function(vnode){
        // this.map.invalidateSize();
    },
    onremove: function(){
        this.group.eachLayer(function(l){
            l.editing.disable();
            this.group.removeLayer(l);
        }, this);
        this.map.remove()
    },
    view: function(vnode){
        return m("div", Object.assign({style: {height: '100%', width: '100%'}}, vnode.attrs));
    }
};
