(function(){

L.Mount = {};
L.Mount = {}
L.Mount.MapLinking = {}

let u = L.Mount.util = {};

u.notImplemented = function(){
    throw new Error("not configured for implementation")
}

u.ensureAdded = function(array, item){
    for(var i = 0; i < array.length; i++){
        if (array[i] === item) {
            return null;
        }
    }
    array.push(item);
    return null;
}

u.ensureRemoved = function(array, item){
    for(var i = 0; i < array.length; i++){
        if (array[i] === item) {
            array.splice(i, 1);
        }
    }
    return null;
}

u.isContained = function(array, item){
    for(var i = 0; i < array.length; i++){
        if (array[i] === item) {
            return true;
        }
    }
    return false;
}

L.Mount.MapLinking.Notifier = {
    _followers: [],
    _eachFollower: function(f){
        this._followers.forEach(f);
        return null;
    },
    _ensureRemoveFollower: function(follower){
        if (u.isContained(this._followers, follower)) {
            follower._idempotentNotifyUnlink();
            follower._notifier = null;
            u.ensureRemoved(this._followers, follower);
        }
        return null;
    },
    _ensureAddFollower: function(follower){
        if (u.isContained(this._followers, follower)) {
            return null;
        }
        if (follower._notifier && follower._notifier !== this) {
            follower._notifier._ensureRemoveFollower(follower);
        }
        u.ensureAdded(this._followers, follower);
        follower._notifier = this;
        follower._idempotentNotifyLink();
        return null;
    },
    _removeAllFollowers: function(){
        var self = this;
        this._eachFollower(function(f){
            self._ensureRemoveFollower(f);
        })
    },
    _notifyFollowersLinked: function(){
        var map = this.getMap();
        if (this.isMapReady()) {
            this._eachFollower(function(f){
                f._idempotentNotifyLink(map);
            })
        }
        return null;
    },
    addFollower: function(f){
        this._ensureAddFollower(f);
        return this;
    },
    removeFollower: function(f){
        this._ensureRemoveFollower(f);
        return this;
    },
    // implementation specific
    getMap: u.notImplemented,
    isMapReady: u.notImplemented,
};

L.Mount.MapLinking.Follower = {
    _notifier: null,
    _is_linked: false,
    getMap: function(){
        if (this._notifier && this._notifier.getMap) {
            return this._notifier.getMap();
        } else {
            return null;
        }
    },
    _idempotentNotifyLink: function(){
        var map = this.getMap();
        if (!map || this._is_linked) {
            return null;
        }
        this._handleLink(map);
        this._is_linked = true;
        return null;
    },
    _idempotentNotifyUnlink: function(){
        var map = this.getMap();
        if (!this._is_linked || !map) {
            return null;
        }
        this._handleUnlink(map);
        this._is_linked = false;
        return null;
    },
    _handleLink: function(map){
        this.linkMap(map);
        return null;
    },
    _handleUnlink: function(map){
        this.unlinkMap(map);
        return null;
    },
    follow: function(notifier){
        notifier.addFollower(this);
        return this;
    },
    unfollow: function(notifier){
        notifier.removeFollower(this);
        return this;
    },
    linkMap: function(map){ return null; },
    unlinkMap: function(map){ return null; },
};

L.Mount.Nesting = {};

L.Mount.Nesting.Containable = {
    _nestRootElement: null,
    getRootElement: function () {
        return this._nestRootElement;
    }
};

L.Mount.Nesting.Container = {
    _nestGroupElement: null,
    _nest: function(kid){
        var k;
        var p = this._nestGroupElement;
        if (kid instanceof HTMLElement)
            k = kid;
        if (kid.getRootElement)
            k = kid.getRootElement();
        else if (isString(kid))
            k = new Text(kid);
        if (k)
            p.appendChild(k);
    },
    _unnest: function(kid){
        var k, i;
        var p = this._nestGroupElement;
        if (kid instanceof HTMLElement)
            k = kid;
        if (kid.getRootElement)
            k = kid.getRootElement();
        else if (isString(kid))
            for (i in p.childNodes)
                if (p.childNodes[i] instanceof Text)
                    if (p.childNodes[i].textContent === kid)
                        k = p.childNodes[i];
        if (k && k.parentNode === p)
            p.removeChild(k);
    }
};

L.Map
 .include(L.Mount.MapLinking.Notifier)
 .include(L.Mount.Nesting.Container)
 .mergeOptions({
     mountContainerClass: "leaflet-mount-map-container",
     zoomControl: false,
     attributionControl: false,
 })
 .addInitHook(function(){
     var classes = this.options.mountContainerClass;
     var container = this._container;
     this._nestGroupElement = L.DomUtil.create("div", classes, container);
     this.whenReady(this._notifyFollowersLinked, this);
     this.on("unload", this._removeAllFollowers, this);
 })
 .include({
     getMap: function(){ return this; },
     isMapReady: function(){ return this._loaded; },
     mount: function(kid){
         this._nest(kid);
         this._ensureAddFollower(kid);
         return this;
     },
     unmount: function(kid){
         this._unnest(kid);
         this._ensureRemoveFollower(kid);
         return this;
     },
 });

L.Mount.Container = L.Class.extend({
    includes: [L.Mount.MapLinking.Notifier,
               L.Mount.MapLinking.Follower,
               L.Mount.Nesting.Containable,
               L.Mount.Nesting.Container],
    initialize: function(){
        var els = this.acquireElement();
        this._nestGroupElement = els.group;
        this._nestRootElement = els.root;
    },
    acquireElement: function(){
        var el = L.DomUtil.create('div', null, null);
        return {
            root: el,
            group: el,
        };
    },
    _handleLink: function(map){
        this.linkMap(map);
        this._eachFollower(function(f){
            f.linkMap(map)
        })
    },
    _handleUnlink: function(map){
        this._eachFollower(function(f){
            f.unlinkMap(map)
        })
        this.unlinkMap(map);
    },
    mount: function(kid){
         this._nest(kid);
         this._ensureAddFollower(kid);
         return this;
    },
    unmount: function(kid){
         this._unnest(kid);
         this._ensureRemoveFollower(kid);
         return this;
    },
});

L.Mount.Leaf = L.Class.extend({
    includes: [L.Mount.MapLinking.Follower,
               L.Mount.Nesting.Containable],
    initialize: function(){
        var els = this.acquireElement();
        // to follow the pattern from L.Mount.Container
        this._nestRootElement = els.root || els;
    },
    acquireElement: function(){
        var el = L.DomUtil.create('div', null, null);
        return {root: el};
    },
    _handleLink: function(map){
        this.linkMap(map);
        return null;
    },
    _handleUnlink: function(map){
        this.unlinkMap(map);
        return null;
    },
});

L.Mount.DomRoot = L.Mount.Container.extend({
    initialize: function(cfg){
        if (cfg instanceof HTMLElement) {
            this._nestGroupElement = cfg;
        } else {
            throw new Error("configuration was not an HTMLElement");
        }
    },
    linkMap: function(map){
        map.addFollower(this);
    },
});

L.Mount.UnsafeLayerAttribution = L.Mount.Leaf.extend({
    options: {
        messages: ['<a href="http://leafletjs.com">Leaflet</a>'],
        delimit: ' | ',
    },
    linkMap: function(map){
        map.on('layeradd layerremove', this._updateDom, this);
        this._updateDom();
    },
    unlinkMap: function(map){
        map.off('layeradd layerremove', this._updateDom, this);
    },
    _updateDom: function(map){
        var i, c, l;
        var span = this.getRootElement();
        var msgs = this.options.messages.slice();
        L.DomUtil.empty(span);
        for (i in map._layers) {
            l = map._layers[i]
            if (l.getAttribution && l.getAttribution().trim().length)
                msgs.push(l.getAttribution())
        }
        for (i in msgs) {
            var c = L.DomUtil.create('cite', '', span);
            if (i < (msgs.length - 1))
                span.appendChild(new Text(this.options.delimit));
            if (this.options.safe)
                c.textContent = msgs[i];
            else
                c.innerHTML = msgs[i];
        }
    },
});

/*
 *   let tm = this.map = L.prebuilt.maps.empty(vnode.dom);
 *   let trails = L.prebuilt.layers.trails();
 *   let streets = L.prebuilt.layers.streets();
 *   let satellite1 = L.prebuilt.layers.satellite();
 *   let satellite2 = L.prebuilt.layers.hereSatellite();
 *   let hybrid = L.prebuilt.layers.hereHybrid();
 *   let topo = L.prebuilt.layers.topo();
 *   let bing = L.prebuilt.layers.bing();
 *   let baseSelect = new L.Mount.BaseLayerSelect();
 *   tm.mount(baseSelect);
 *   tm.addLayer(hybrid);
 *   baseSelect
 *       .manageLayer({title: "bing",        layer: bing})
 *       .manageLayer({title: "streets",     layer: streets})
 *       .manageLayer({title: "satellite 1", layer: satellite1})
 *       .manageLayer({title: "satellite 2", layer: satellite2})
 *       .manageLayer({title: "hybrid",      layer: hybrid})
 *       .manageLayer({title: "topo",        layer: topo})
 *       .manageLayer({title: "trails",      layer: trails});
 *
 * support `add` key:
 *
 *   let tm = this.map = L.prebuilt.maps.empty(vnode.dom);
 *   let baseSelect = new L.Mount.BaseLayerSelect();
 *   tm.mount(baseSelect);
 *   tm.addLayer(hybrid);
 *   baseSelect
 *       .manageLayer({title: "bing",        layer: L.prebuilt.layers.bing()                   })
 *       .manageLayer({title: "streets",     layer: L.prebuilt.layers.streets()                })
 *       .manageLayer({title: "satellite 1", layer: L.prebuilt.layers.satellite()              })
 *       .manageLayer({title: "satellite 2", layer: L.prebuilt.layers.hereSatellite()          })
 *       .manageLayer({title: "hybrid",      layer: L.prebuilt.layers.hereHybrid(),   add: true})
 *       .manageLayer({title: "topo",        layer: L.prebuilt.layers.topo()                   })
 *       .manageLayer({title: "trails",      layer: L.prebuilt.layers.trails()                 });
 */
L.Mount.BaseLayerSelect = L.Mount.Leaf.extend({
    initialize: function(){
        L.Mount.Leaf.prototype.initialize.call(this);
        this._layerStore = {};
    },
    acquireElement: function(){
        return {
            root: L.DomUtil.create('select', null, null),
        };
    },
    manageLayer: function(cfg){
        var layer = cfg.layer;
        var title = cfg.title;
        var id = L.stamp(layer)
        var select = this.getRootElement();
        var opt = L.DomUtil.create("option", "", select);
        opt.value = id;
        opt.innerText = title;
        layer.on("add remove", this._updateDom, this);
        this._layerStore[id] = {
            layer: layer,
            title: title,
            option: opt,
        };
        this._updateDom();
        return this;
    },
    forgetLayer: function(cfg){
        var layer = cfg.layer;
        var id = L.stamp(cfg.layer);
        var store = this._layerStore;
        var select = this.getRootElement();
        layer.off("add remove", this._updateDom, this);
        if (store[id]) {
            L.DomUtil.remove(store[id].option);
        }
        this._updateDom();
        return this;
    },
    _updateDom: function(){
        var id;
        var map = this.getMap();
        var store = this._layerStore;
        if (!map) return null;
        for (id in store) {
            if (map.hasLayer(store[id].layer)) {
                store[id].option.setAttribute("selected", "");
            } else {
                store[id].option.removeAttribute("selected");
            }
        }
    },
    _setLayerByEvent: function(event){
        var id;
        var targetId = event.target.value;
        var map = this.getMap();
        var store = this._layerStore;
        for (id in store) {
            if (targetId === id) {
                map.addLayer(store[id].layer);
            } else {
                map.removeLayer(store[id].layer);
            }
        }
        this._updateDom();
    },
    linkMap: function(){
        var el = this._nestRootElement;
        L.DomEvent.disableClickPropagation(el);
        L.DomEvent.addListener(el, 'change', this._setLayerByEvent, this);
        this._updateDom();
    },
    unlinkMap: function(){
        var id;
        var store = this._layerStore;
        var el = this._nestRootElement;
        L.DomEvent.removeListener(el, 'change', this._setLayerByEvent, this);
        for (id in store) {
            store[id].layer.off("add remove", this._updateDom, this);
        }
    },
});

// TODO: follow the map's zoomDelta
L.Mount.IncreaseZoomBy1 = L.Mount.Leaf.extend({
    acquireElement: function(){
        var btn = L.DomUtil.create("button");
        L.DomEvent.disableClickPropagation(btn)
        var text = "zoom in";
        btn.innerText = text;
        return {
            root: btn,
        }
    },
    _canZoom: function(){
        var map = this.getMap();
        return (map.getZoom() + 1) <= map.getMaxZoom()
    },
    _zoomMap: function(){
        var map = this.getMap();
        if (this._canZoom()) {
            map.setZoom(map.getZoom() + 1)
        }
        return null
    },
    _updateDom: function(){
        if (this._canZoom()) {
            this.getRootElement().removeAttribute("disabled");
        } else {
            this.getRootElement().setAttribute("disabled", "");
        }
        return null;
    },
    linkMap: function(map){
        var el = this.getRootElement();
        L.DomEvent.addListener(el, "click", this._zoomMap, this);
        this.getMap().on("zoomend", this._updateDom, this);
        this._updateDom();
    },
    unlinkMap: function(map){
        var el = this.getRootElement();
        L.DomEvent.addListener(el, "click", this._zoomMap, this);
        this.getMap().off("zoomend", this._updateDom, this);
    },
})

L.Mount.DecreaseZoomBy1 = L.Mount.Leaf.extend({
    acquireElement: function(){
        var btn = L.DomUtil.create("button");
        L.DomEvent.disableClickPropagation(btn)
        var text = "zoom out";
        btn.innerText = text;
        return {
            root: btn,
        }
    },
    _canZoom: function(){
        var map = this.getMap();
        return (map.getZoom() - 1) >= map.getMinZoom()
    },
    _zoomMap: function(){
        var map = this.getMap();
        if (this._canZoom()) {
            map.setZoom(map.getZoom() - 1)
        }
        return null
    },
    _updateDom: function(){
        if (this._canZoom()) {
            this.getRootElement().removeAttribute("disabled");
        } else {
            this.getRootElement().setAttribute("disabled", "");
        }
        return null;
    },
    linkMap: function(map){
        var el = this.getRootElement();
        L.DomEvent.addListener(el, "click", this._zoomMap, this);
        this.getMap().on("zoomend", this._updateDom, this);
        this._updateDom();
    },
    unlinkMap: function(map){
        var el = this.getRootElement();
        L.DomEvent.addListener(el, "click", this._zoomMap, this);
        this.getMap().off("zoomend", this._updateDom, this);
    },
});

L.Mount.LayerToggle = L.Mount.Leaf.extend({
    initialize: function(layer){
        L.Mount.Leaf.prototype.initialize.call(this);
        this._layer = layer;
    },
    acquireElement: function(){
        var checkbox = L.DomUtil.create("input", null, l);
        checkbox.type = "checkbox";
        return {
            root: checkbox,
        };
    },
    _updateDom: function(){
        var isAttached = this.getMap().hasLayer(this._layer);
        this.getRootElement().checked = isAttached;
    },
    _toggleLayer: function(){
        var map = this.getMap();
        var layer = this._layer;
        if (map.hasLayer(layer)) {
            map.removeLayer(layer);
        } else {
            map.addLayer(layer);
        }
        return null;
    },
    linkMap: function(map){
        var checkbox = this.getRootElement();
        var layer = this._layer;
        L.DomEvent.addListener(checkbox, "click", this._toggleLayer, this);
        layer.on("add remove", this._updateDom, this);
        this._updateDom();
    },
    unlinkMap: function(map){
        var checkbox = this.getRootElement();
        var layer = this.options.layer;
        L.DomEvent.removeListener(checkbox, "click", this._toggleLayer, this);
        layer.off("add remove", this._updateDom, this);
    },
});

L.Mount.TagContainer = L.Mount.Container.extend({
    options: {
        tag: "div",
        classes: [],
        id: null,
        config: function(){},
    },
    initialize: function(opts){
        L.Util.setOptions(this, opts);
        L.Mount.Leaf.prototype.initialize.call(this);
    },
    acquireElement: function(){
        var opt = this.options;
        var el = L.DomUtil.create(opt.tag, opt.class, null);
        opt.config(el);
        if (opt.id) {
            el.setAttribute("id", opt.id);
        }
        return {
            root: el,
            group: el,
        };
    },
});

})();

