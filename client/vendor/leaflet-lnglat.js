(function(){

//TODO: add support for Bounds

function cantFlip(obj){
    return "L.lnglat cannot flip coordinates of type: " + JSON.stringify(obj);
}

function yx(v){
    var first, err;
    if (Array.isArray(v)) {
        first = v[0];
        if (typeof first === "number") {
            return {lat: v[1], lng: v[0]};
        } else {
            return v.map(yx);
        }
    } else if (typeof v === "object") {
        return v;
    } else {
        throw new Error(cantFlip(v));
    }
}

function xy(v){
    var err, first;
    if (Array.isArray(v)) {
        first = v[0];
        if (typeof first === "number") {
            return v;
        } else {
            return v.map(xy);
        }
    } else if (typeof v === "object") {
        return [v.lng, v.lat];
    } else {
        throw new Error(cantFlip(v));
    }
}

L.lnglat = {}
L.lnglat.yx = yx
L.lnglat.xy = xy

/*
 * TODO: maybe add some factory functions here so you can do
 *
 * `L.lnglat.polygon(coords, opts)`
 *
 * instead of the hacky-looking
 *
 * `L.polygon([], opts).setLngLats(points)`
 *
 */

L.lnglat.extensions = {

    Map: {
        setViewLngLat: function(center, zoom){
            return this.setView(yx(center), zoom);
        },
        getCenterLngLat: function(){
            return xy(this.getCenter())
        },
        fitBoundsLngLat: function(bounds){
            return this.fitBounds(yx(bounds));
        },
    },

    Marker: {
        setLngLat: function(center){
            return this.setLatLng(yx(center))
        },
        getLngLat: function(){
            return xy(this.getLatLng())
        },
    },

    Circle: {
        setLngLat: function(center){
            return this.setLatLng(yx(center))
        },
        getLngLat: function(){
            return xy(this.getLatLng())
        },
    },


    CircleMarker: {
        setLngLat: function(center){
            return this.setLatLng(yx(center))
        },
        getLngLat: function(){
            return xy(this.getLatLng())
        },
    },

    Polyline: {
        setLngLats: function(points){
            return this.setLatLngs(yx(points))
        },
        getLngLats: function(){
            return xy(this.getLatLngs())
        },
    },

};

var extendLeafletBase = function(){
    var a;
    for (a in L.lnglat.extensions) {
        L[a].include(L.lnglat.extensions[a])
    }
    return null;
}

L.lnglat.extendL = extendLeafletBase;

})()
