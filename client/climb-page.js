pages.Climb = {
    oninit: function(vnode){
        this.climb = null;
        this.photos = [];
        m.request('/climbs/' + vnode.attrs.id).then(a => this.climb = a).then(_ => {
            m.request('/photos').then((photos) => {
                for (let photo of photos)
                    if (photo.climbLayers?.find(co => co.climbUuid === this.climb.uuid))
                        this.photos.push(photo)
            });
        });
    },
    save: util.debounce(1000, function(){
        for (let k in this.climb)
            if (this.climb[k] === null)
                delete this.climb[k];
        m.request({
            method: 'PUT',
            url: '/climbs/:id',
            params: {id: this.climb.uuid},
            body: this.climb,
        });
    }),
    view: function(vnode) {
        document.title = this.climb ? (this.climb.name || 'climb unnamed') : 'climb loading';
        const mpId = this.climb?.fields?.mountainproject_id;
        const c = this.climb;
        return !c ? m('p', 'loading') : m('article.climb-page', [
            m('.evp', [
              m('.evp__prompt', [
                'Internal Id',
              ]),
              m('.evp__edit', [
                m('.uuid', m(components.climb.UUID, {
                  value: c.uuid
                })),
              ]),
              m('.evp__view', [
                m('small.mono', c.uuid)
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Name(s)',
              ]),
              m('.evp__edit', [
                m(components.climb.Name, {
                  value: c.name,
                  onchange: n => { c.name = n; this.save() },
                }),
                m(components.climb.AlternateNames, {
                  value: c.alternateNames,
                  onchange: a => { c.alternateNames = a; this.save() },
                }),
              ]),
              m('.evp__view', [
                m('.climb-name', c.name),
                m('ul.climb-alternate-names', (c.alternateNames || []).map(n => {
                  return m('li.climb-alternate-name', n);
                })),
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Grade & Style',
              ]),
              m('.evp__edit', [
                m(components.climb.Rating, {
                  value: c.rating,
                  onchange: v => { c.rating = v; this.save() },
                }),
                m(components.climb.ProjectStatus, {
                  value: c.projectStatus,
                  onchange: v => { c.projectStatus = v; this.save() },
                }),
              ]),
              m('.evp__view', [
                  m('.rating', [
                    m('.rating__difficulty-protection', [
                      m('span', {
                        style: c.projectStatus ? {filter: 'blur(1.5px)'} : {},
                      }, c.rating?.difficulty),
                      m('span', c.rating?.protection),
                    ]),
                    m('div', c.rating?.style),
                  ]),
                  m('.project-status', [
                    c.projectStatus && `project, ${c.projectStatus}`
                  ]),
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Length'
              ]),
              m('.evp__edit', [
                m('.length', m(components.climb.Length, {
                  value: c.length,
                  onchange: v => { c.length = v; this.save() },
                })),
              ]),
              m('.evp__view', [
                c.length && ((c.length.lowerEstimate === c.length.upperEstimate) ?
                             `${c.length.lowerEstimate} ${c.length.unit}` :
                             `${c.length.lowerEstimate}~${c.length.upperEstimate} ${c.length.unit}`)
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Ascents & Events',
              ]),
              m('.evp__edit', [
                m('.ascents', m(components.climb.Ascents, {
                  value: c.ascents,
                  onchange: a => { c.ascents = a; this.save() },
                })),
              ]),
              m('.evp__view', [
                m('ol.climb-ascents', [(c.ascents || []).map(a => 
                  m('li.climb-ascent', a))
                ])
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Tags',
              ]),
              m('.evp__edit', [
                m(components.climb.Tags, {
                  value: c.tags,
                  onchange: v => { c.tags = v; this.save() },
                }),
              ]),
              m('.evp__view', [
                m('ul.climb-tags', [(c.tags || []).map(t => 
                  m('li.climb-tag', t))
                ])
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Fields',
              ]),
              m('.evp__edit', [
                m(components.climb.Fields, {
                  value: c.fields,
                  onchange: a => { c.fields = a; this.save() },
                }),
              ]),
              m('.evp__view', [
                m('.climb-fields', [
                  Object.entries(c.fields || {}).map(([k, v]) => 
                    m('.climb-field', [
                      m('.climb-field-key', k),
                      m('.climb-field-value', v),
                    ]))
                ]),
                m('aside.links', m('ul', [
                    mpId ? m('li', m('a', {
                        href: `https://www.mountainproject.com/route/${mpId}`,
                        target: '_blank'
                    }, 'Mountain Project')) : null,
                ])),
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Notes',
              ]),
              m('.evp__edit', [
                m('.notes', m(components.climb.Notes, {
                  value: c.notes,
                  onchange: v => { c.notes = v; this.save() },
                })),
              ]),
              m('.evp__view', [
                c.notes?.map(n => m('.climb-note', [
                   m('.climb-note-topic', n.topic),
                   m('.climb-note-content', n.content),
                ]))
              ]),
            ]),
            m('.evp', [
              m('.evp__prompt', [
                'Links'
              ]),
              m('.evp__edit', [
                m('.resources', m(components.climb.Resources, {
                  value: c.resources,
                  onchange: v => { c.resources = v; this.save() },
                })),
              ]),
              m('.evp__view', [
                c.resources?.map(r => m('.climb-resource', [
                   m('a', {
                     href: r.resource,
                   }, r.title || r.resource),
                   r.description && m('.climb-resource-description', r.description),
                ]))
              ]),
            ]),
            m('.climb-page--steepness-terrain', [
              m('.steepness', m(components.climb.Steepness, {
                value: c.steepness,
                onchange: v => { c.steepness = v; this.save() },
              })),
              m('.terrain', m(components.climb.Terrain, {
                value: c.terrain,
                onchange: v => { c.terrain = v; this.save() },
              })),
            ]),
            m('.location', m(components.climb.Location, {
              value: c.location,
              onchange: v => { c.location = v; this.save() },
              suggestedBounds: util.roughBounds.getAsLeafletBounds(),
            })),
            m('aside', [
              this.photos.length ? 'appears on photos' : 'not drawn on any photos',
              m('ul', this.photos.map(p => m('li', m(m.route.Link, {href: `/photos/${p.uuid}`}, p.uuid)))),
            ]),
            m('details', [
              m('summary', 'data'),
              m('.data-dump', JSON.stringify(c, null, 2)),
            ]),
            m('button', {
              style: {color: 'white', background: 'red'},
              onclick: () => {
                m.request({
                  url: '/climbs/' + c.uuid,
                  method: 'DELETE',
                }).then(() => m.route.set('/climbs'));
              }
            }, 'delete climb'),
        ]);
    },
};

