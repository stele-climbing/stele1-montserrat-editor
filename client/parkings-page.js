pages.Parkings = {
    oninit: function(){
        document.title = 'Parkings';
        this.parkings = null;
        this.newParkingName = '';
        m.request('/parkings').then(a => this.parkings = a);
    },
    view: function() {
        return m('article.standard', [
            m('h1', 'parkings'),
            m('section', [
              m('input', {
                type: 'text',
                value: this.newParkingName,
                placeholder: 'new parking name',
                oninput: e => this.newParkingName = e.target.value,
              }),
              m('button', {
                disabled: !this.newParkingName.trim(),
                onclick: e => {
                  m.request({
                    method: 'POST',
                    url: '/parkings',
                    body: {name: this.newParkingName}
                  }).then(_ => {
                    m.request('/parkings').then(a => this.parkings = a);
                  });
                }
              }, 'add parking'),
            ]),
            m('ul', (this.parkings || []).map(function(a){ 
                return m('li', [
                    a.name ? m(m.route.Link, {href: '/parkings/' + a.uuid}, a.name)
                           : m(m.route.Link, {style: {opacity: 0.5}, href: '/parkings/' + a.uuid}, '<UNNAMED>')
                ]);
            })),
        ]);
    },
};
