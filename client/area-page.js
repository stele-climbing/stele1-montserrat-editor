pages.Area = {
    oninit: function(vnode){
        this.area = null;
        m.request('/areas/' + vnode.attrs.id).then(a => this.area = a);
    },
    save: util.debounce(1000, function(){
        Object.keys(this.area).forEach(k => {
            if (this.area[k] === null) delete this.area[k];
        });
        m.request({
            method: 'PUT',
            url: '/areas/:id',
            params: {id: this.area.uuid},
            body: this.area,
        });
    }),
    view: function(vnode) {
        document.title = this.area?.name || '[AREA UNNAMED]';
        return !this.area ? m('p', 'loading') : m('article.area-page.standard', [
            m('h1', 'edit area'),
            m('.name', m(components.area.Name, {
              value: this.area.name,
              onchange: n => { this.area.name = n; this.save() },
            })),
            m('.uuid', m(components.area.UUID, {
              value: this.area.uuid
            })),
            m('.alternate-names', m(components.area.AlternateNames, {
              value: this.area.alternateNames,
              onchange: a => { this.area.alternateNames = a; this.save() },
            })),
            m('.fields', m(components.area.Fields, {
              value: this.area.fields,
              onchange: a => { this.area.fields = a; this.save() },
            })),
            m('.notes', m(components.area.Notes, {
              value: this.area.notes,
              onchange: v => { this.area.notes = v; this.save() },
            })),
            m('.tags', m(components.area.Tags, {
              value: this.area.tags,
              onchange: v => { this.area.tags = v; this.save() },
            })),
            m('.location', m(components.area.Location, {
              value: this.area.location,
              onchange: v => { this.area.location = v; this.save() },
            })),
            m('.summary', {
                style: {border: 'solid 1px black', 'white-space': 'pre'}
            }, JSON.stringify(this.area, null, 2)),
            m('button', {
              style: {color: 'white', background: 'red'},
              onclick: () => {
                m.request({
                  url: '/areas/' + this.area.uuid,
                  method: 'DELETE',
                }).then(() => m.route.set('/areas'));
              }
            }, 'delete area'),
        ]);
    },
};

