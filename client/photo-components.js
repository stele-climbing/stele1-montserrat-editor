window.components.photo = {};

window.components.photo.ClimbLayers = (function(){
let midpoints = function(a){
    if (a.length<=1) return [];
    let midpoint = function(a, b){
        let low = Math.min(a, b);
        let high = Math.max(a, b);
        return low + ((high - low) / 2);
    }
    let b = [];
    for (let i=1;i<a.length;i++) {
        b.push({
            topOffset: midpoint(a[i].topOffset, a[i-1].topOffset),
            leftOffset: midpoint(a[i].leftOffset, a[i-1].leftOffset)
        });
    }
    return b;
};
let addPoint = function(a, uuid, pt){
    let b = util.copy(a);
    return b.map(function(v){
        if (v.climbUuid !== uuid)
            return v;
        else
            return {
                climbUuid: uuid,
                path: v.path.concat([pt]),
            };
    });
};
let removePoint = function(a, uuid, idx){
    let b = util.copy(a);
    return b.map(function(v){
        if (v.climbUuid !== uuid) return v;
        else {
            return {
                climbUuid: uuid,
                path: (v.path.length > 2) ? v.path.filter((v, i) => i !== idx) : v.path,
            };
        }
    });
};
function updateSvg(vnode, el){
    if (!vnode.state.size) return;
    let state = vnode.state;
    let onchange = v => {vnode.attrs.onchange(v); m.redraw()};
    let svg = el.svg;
    let size = state.size;
    let v = vnode.attrs.value || [];
    let y = d3.scaleLinear().domain([0, 1]).range([0, size.height]);
    let x = d3.scaleLinear().domain([0, 1]).range([0, size.width]);
    let line = d3.line().x(v => x(v.leftOffset)).y(v => y(v.topOffset));
    svg.attr('viewBox', `0 0 ${size.width} ${size.height}`)
        .on('click', function(e){
            let svgc = d3.pointer(e, svg.node());
            let newpoint = {leftOffset: x.invert(svgc[0]), topOffset: y.invert(svgc[1])};
            onchange(addPoint(v, state.editing, newpoint));
        });
    el.image.attr('x', 0).attr('y', 0)
        .attr('width', size.width)
        .attr('height', size.height);
    el.lines.selectAll('path').data(v, v => v.climbUuid)
        .join('path')
        .attr('stroke', 'orange')
        .attr('stroke-width', 5)
        .attr('fill', 'none')
        .attr('d', d => line(d.path))
        .on('click', function(e, d){
            e.stopPropagation();
            state.editing = d.climbUuid;
            m.redraw();
        });
    let handlePoints = state.editing ? v.find(l => l.climbUuid === state.editing).path : [];
    el.handles.selectAll('circle').data(handlePoints.map((p, i) => ({point: p, index: i})))
        .join('circle')
        .attr('r', 9)
        .attr('fill', 'yellow')
        .attr('stroke', 'none')
        .attr('cx', d => x(d.point.leftOffset))
        .attr('cy', d => y(d.point.topOffset))
        .on('click', function(e, d){
            e.stopPropagation();
            onchange(removePoint(v, state.editing, d.index));
        });
};
return {
    oninit: function(vnode){
        let v = vnode.attrs.value;
        this.editing = (v && v[0]) ? v[0].climbUuid : null; // TODO: remove after testing
        this.search = '';
        this.imageUrl = `/photos/${vnode.attrs.photoUuid}.jpg?covering_square=600`;
        util.imageDimensions(this.imageUrl).then(f => {
            this.size = f;
            m.redraw();
        });
    },
    view: function(vnode){
        let state = this;
        let v = vnode.attrs.value || [];
        let imageUrl = this.imageUrl;
        const climbs = vnode.attrs.climbs || [];
        return m('figure.photo-climb-layers', {style: {backgroundColor: 'tan', padding: '0.5em'}}, [
            m('svg', {
                oncreate: function(vn){
                    this.elements = {};
                    let svg = this.elements.svg = d3.select(vn.dom);
                    this.elements.image = svg.append('image').attr('href', imageUrl);
                    this.elements.lines = svg.append('g').classed('lines', true);
                    this.elements.handles = svg.append('g').classed('handles', true);
                    this.elements.midhandles = svg.append('g').classed('midhandles', true);
                    updateSvg(vnode, this.elements);
                },
                onupdate: function(vn){
                    updateSvg(vnode, this.elements);
                },
            }),
            m('figcaption', {style: {display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between'}}, [
                m('ul', v.map(function(l){
                    return m('li', [
                        m('span', (l.climbUuid === state.editing) ? '✎' : ' '),
                        m('button', {
                            onclick: _ => state.editing = (l.climbUuid === state.editing) ? null : l.climbUuid,
                        }, climbs.find(c => c.uuid === l.climbUuid)?.name),
                        m('button', {
                            onclick: function(){
                                vnode.attrs.onchange(v.filter(a => a.climbUuid !== l.climbUuid));
                                if (state.editing === l.climbUuid) state.editing = null;
                            }
                        }, m.trust('&times;'))
                    ]);
                })),
                m('.photo-climb-layers--add-climb', [
                    m('input', {
                        value: state.search,
                        oninput: e => state.search = e.target.value,
                    }),
                    m('ul', this.search ? climbs.filter(function(c){
                        let matchesSearch = s => s.indexOf(state.search) > -1;
                        return matchesSearch(c.name) || (c.alternateNames || []).find(matchesSearch);
                    }).map(function(c){
                        let added = v.map(l => l.climbUuid).indexOf(c.uuid) >= 0;
                        return m('li', m('button', {
                            disabled: added ? 'disabled' : null,
                            onclick: function(){
                                if (!added) {
                                    let b = util.copy(v);
                                    state.editing = c.uuid;
                                    vnode.attrs.onchange(b.concat([{
                                        climbUuid: c.uuid,
                                        path: [
                                            {topOffset: 0.75, leftOffset: 0.75},
                                            {topOffset: 0.25, leftOffset: 0.25},
                                        ]
                                    }]));
                                }
                            }
                        }, '+ ' + c.name));
                    }) : []),
                ]),
            ]),
        ]);
    },
};
})();

window.components.photo.AreaLayers = (function(){
function midpoints(a){
    if (a.length<=1) return [];
    let midpoint = function(a, b){
        let low = Math.min(a, b);
        let high = Math.max(a, b);
        return low + ((high - low) / 2);
    }
    let b = [];
    for (let i=1;i<a.length;i++) {
        b.push({
            topOffset: midpoint(a[i].topOffset, a[i-1].topOffset),
            leftOffset: midpoint(a[i].leftOffset, a[i-1].leftOffset)
        });
    }
    return b;
};
function removePoint(original, layerUuid, index){
    let b = util.copy(original);
    return b.map(function(l){
        if (l.areaUuid !== layerUuid) return l;                // leave other layers as-is
        if (l.polygon.length <= 4) return l;                   // can't reduce a triangle
        if (index === 0 || index === (l.polygon.length-1)) {   // first and last points must both be removed
            let startCopy = util.copy(l.polygon[1]);           //   and duplicated
            return {
                areaUuid: l.areaUuid,
                polygon: l.polygon.slice(1, l.polygon.length-1).concat([startCopy]),
            };
        } else {                                               // cut out the point
            return {
                areaUuid: l.areaUuid,
                polygon: l.polygon.slice(0, index).concat(l.polygon.slice(index+1)),
            };
        }
    });
};
function updatePoint(original, layerUuid, index, pt){
    let b = util.copy(original);
    return b.map(function(l){
        if (l.areaUuid !== layerUuid) return l;
        if (index === 0 || index === (l.polygon.length-1)) {
            l.polygon[0] = util.copy(pt);
            l.polygon[l.polygon.length-1] = util.copy(pt);
            return {
                areaUuid: l.areaUuid,
                polygon: l.polygon,
            };
        } else {
            l.polygon[index] = util.copy(pt);
            return {
                areaUuid: l.areaUuid,
                polygon: l.polygon,
            };
        }
    });
};
function insertPoint(original, layerUuid, index, pt){
    let b = util.copy(original);
    return b.map(function(l){
        if (l.areaUuid === layerUuid)
            l.polygon.splice(index+1, 0, util.copy(pt));
        return l;
    });
};
function updateSvg(vnode, elements){
    if (!vnode.state.imageUrl || !vnode.state.size) return;
    let s = vnode.state;
    let svg = elements.svg;
    let ghandles = elements.handles;
    let gmidhandles = elements.midhandles;
    let glayers = elements.layers;
    let layers = vnode.attrs.value || [];
    let onchange = v => { vnode.attrs.onchange(v); m.redraw() };
    let x = d3.scaleLinear().domain([0, 1]).range([0, s.size.width]).clamp(true);
    let y = d3.scaleLinear().domain([0, 1]).range([0, s.size.height]).clamp(true);
    let line = d3.line().x(v => x(v.leftOffset)).y(v => y(v.topOffset));
    let offsets = e => {
        let p = d3.pointer(e, svg.node());
        return {leftOffset: x.invert(p[0]), topOffset: y.invert(p[1])};
    };
    elements.image.attr('href', s.imageUrl).attr('x', 0).attr('y', 0)
        .attr('width', s.size.width)
        .attr('height', s.size.height);
    svg.attr('viewBox', `0 0 ${s.size.width} ${s.size.height}`);
    glayers.selectAll('path').data(layers)
        .join('path')
        .attr('d', d => line(d.polygon))
        .attr('stroke', 'pink')
        .attr('fill', 'none')
        .attr('stroke-width', 5)
        .on('click', function(e, d){
            s.editing = d.areaUuid;
            m.redraw();
        });
    let handles = s.editing ? layers.find(l => l.areaUuid === s.editing).polygon : [];
    ghandles.selectAll('circle').data(handles.map((p, i) => ({point: p, index: i})))
        .join('circle')
        .attr('cx', d => x(d.point.leftOffset))
        .attr('cy', d => y(d.point.topOffset))
        .attr('r', 9)
        .attr('fill', 'yellow')
        .attr('stroke', 'none')
        .on('click', function(sel, data){
            let removed = removePoint(layers, s.editing, data.index);
            onchange(removePoint(layers, s.editing, data.index));
        })
        .call(d3.drag().on('drag', function(e, data){
            onchange(updatePoint(layers, s.editing, data.index, offsets(e)));
        }));
    gmidhandles.selectAll('circle')
        .data(midpoints(handles).map((p, i) => ({point: p, index: i})))
        .join('circle')
        .attr('cx', d => x(d.point.leftOffset))
        .attr('cy', d => y(d.point.topOffset))
        .attr('r', 7)
        .attr('fill', 'orange')
        .attr('stroke', 'none')
        .call(d3.drag().on('drag', function(e, data){
            onchange(insertPoint(layers, s.editing, data.index, offsets(e)));
        }));
};
return {
    oninit: function(vnode){
        this.editing = vnode.attrs.value ? vnode.attrs.value[0].areaUuid : null;
        this.imageUrl = `/photos/${vnode.attrs.photoUuid}.jpg?covering_square=600`;
        util.imageDimensions(this.imageUrl).then(f => {
            this.size = f;
            m.redraw();
        });
    },
    view: function(vnode){
        let state = this;
        let v = vnode.attrs.value || [];
        let layers = vnode.attrs.layers || [];
        let areas = vnode.attrs.areas || [];
        let nameById = function(uuid){
            let res = areas.find(a => a.uuid === uuid);
            return res ? res.name : null;
        };
        return m('figure.photo-area-layers', {style: {backgroundColor: 'lightgreen', padding: '0.5em'}}, [
            m('svg', {
                oncreate: function(vn){
                    let e = this.elements = {};
                    e.svg = d3.select(vn.dom);
                    e.image = e.svg.append('image').attr('href', this.imageUrl);
                    e.layers = e.svg.append('g').classed('layers', true);
                    e.midhandles = e.svg.append('g').classed('midhandles', true);
                    e.handles = e.svg.append('g').classed('handles', true);
                    updateSvg(vnode, this.elements);
                },
                onupdate: function(svgVnode){
                    updateSvg(vnode, this.elements);
                },
            }),
            m('figcaption', [
                m('ul.photo-area-layers--layer-controls', v.map(function(l){
                    return m('li', [
                        m('span.photo-area-layers--editing-status', (state.editing === l.areaUuid) ? '✎ ' : ' '),
                        m('button.photo-area-layers--layer-control', {
                            onclick: function(){
                                state.editing = (l.areaUuid !== state.editing) ? l.areaUuid : null;
                            }
                        }, nameById(l.areaUuid)),
                        m('button.photo-area-layers--layer-remove', {
                            onclick: function(){
                                let b = util.copy(v).filter(layer => layer.areaUuid !== l.areaUuid);
                                if (l.areaUuid === state.editing) state.editing = null;
                                vnode.attrs.onchange(b);
                            }
                        }, m.trust('&times;'))
                    ]);
                })),
                m('.photo-area-layers--add-layer', [
                    m('ul.photo-area-layers--add-layer-results', vnode.attrs.areas.map(function(a){
                        return m('button.photo-area-layers--add-layer-button', {
                            disabled: v.find(added => added.areaUuid === a.uuid) ? 'disabled' : null,
                            onclick: function(){
                                vnode.attrs.onchange(util.copy(v).concat([{
                                    areaUuid: a.uuid,
                                    polygon: [
                                        {leftOffset: 0.75, topOffset: 0.75},
                                        {leftOffset: 0.25, topOffset: 0.75},
                                        {leftOffset: 0.25, topOffset: 0.25},
                                        {leftOffset: 0.75, topOffset: 0.25},
                                        {leftOffset: 0.75, topOffset: 0.75},
                                    ],
                                }]));
                                state.editing = a.uuid;
                            }
                        }, '+ ' + a.name);
                    })),
                ]),
            ]),
        ]);
    },
};
})();

window.components.photo.RealDatetimeoriginal = {
    view: function(vnode){
        return m('.photo-real-datetimeoriginal', [
            m('input.photo-real-datetimeoriginal--both-as-text', {
                type: 'text',
                value: vnode.attrs.value,
                placeholder: 'Real DateTimeOriginal',
                oninput: e => {
                    let v = e.target.value;
                    vnode.attrs.onchange(v !== '' ? v : null);
                }
            }),
       ]);
    },
};

//NOTE: i'm not sure this works
window.components.photo.RealOrientation = {
    view: function(vnode){
        return m('.photo-real-orientation', [
            m('span', 'want ⮳ got '),
            m('select', {
                value: vnode.attrs.value,
                placeholder: 'Real Orientation',
                oninput: function(e){
                    let v = e.target.value;
                    vnode.attrs.onchange(util.looksLikeNumber(v) ? Number(v) : null);
                },
            }, [
                {value: null, text: 'unset'},
                {value: 1, text: '1: ⮳'},
                {value: 2, text: '2: ⮲'},
                {value: 3, text: '3: ⮰'},
                {value: 4, text: '4: ⮱'},
                {value: 5, text: '5: ⮶'},
                {value: 6, text: '6: ⮴'},
                {value: 7, text: '7: ⮵'},
                {value: 8, text: '8: ⮷'},
            ].map(o => m('option', {
                selected: o.value === (vnode.attrs.value || null),
                value: o.value,
            }, o.text))),
        ])
    },
};

window.components.photo.UUID = {
    view: function(vnode){
        return m('input.photo-uuid', {
            value: vnode.attrs.value,
            placeholder: 'uuid',
            disabled: true,
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.photo.Fields = {
    oninit: function(vnode){
        this.newField = '';
    },
    newFieldIsValid: function(vnode){
        let keys = vnode.attrs.value ? Object.keys(vnode.attrs.value) : [];
        let keyConflict = 0 <= keys.indexOf(this.newField);
        return (this.newField !== '') && !keyConflict;
    },
    view: function(vnode){
        let kvs =  vnode.attrs.value || {};
        let ks = Object.keys(kvs).sort();
        return m('.photo-fields', [
            m('.photo-fields--new-field', [
                m('input.photo-fields--new-field-input', {
                    value: this.newField,
                    oninput: e => this.newField = e.target.value,
                    placeholder: 'new field'
                }),
                m('button.photo-fields--new-field-add', {
                    disabled: !this.newFieldIsValid(vnode),
                    onclick: a => {
                        let o = util.copy(kvs);
                        o[this.newField] = '';
                        this.newField = '';
                        vnode.attrs.onchange(o);
                    }
                }, 'add'),
            ]),
            ks.map(function(k){
                return m('.photo-fields--field', {key: k.topic}, [
                    m('input.photo-fields--field-name', {
                        value: k,
                        disabled: true,
                    }),
                    m('input.photo-fields--field-value', {
                        value: kvs[k],
                        oninput: e => {
                            let kvs2 = util.copy(kvs);
                            kvs2[k] = e.target.value;
                            vnode.attrs.onchange(kvs2);
                        }
                    }),
                    m('button.photo-fields--field-value', {
                        onclick: e => {
                            let kvs2 = util.copy(kvs);
                            delete kvs2[k];
                            vnode.attrs.onchange(kvs2);
                        },
                    }, 'remove'),
                ]);
            }),
        ]);
    },
};

window.components.photo.RealLocation = {
    view: function(vnode){
        return m('.photo-real-location', {style: {height:'100%', width: '100%'}}, [
           m(common.PointAccuracySetter, {
               getCircle: function(){
                   let v = vnode.attrs.value;
                   return v ? {
                       point: [v.longitude, v.latitude],
                       accuracy: v.meterRadius,
                   } : null;
               },
               setCircle: c => vnode.attrs.onchange(c ? {
                   longitude: c.point[0],
                   latitude: c.point[1],
                   meterRadius: c.accuracy,
               } : null),
               suggestedCenter: vnode.attrs.suggestedCenter,
               suggestedBounds: vnode.attrs.suggestedBounds,
           }),
        ]);
    },
};

window.components.photo.Thumbnail = {
    oncreate: function(vnode){
        this.uuid = vnode.attrs.photo.uuid;
        this.size = vnode.attrs.size;
        this.imageUrl = `/photos/${this.uuid}.jpg?covering_square=${this.size}`;
        this.refresher = window.setInterval(() => {
            const rect = vnode.dom.getBoundingClientRect();
            const vh = Math.max(document.documentElement.clientHeight, window.innerHeight);
            this.visible = !(rect.bottom < 0 || rect.top - vh >= 0);
            m.redraw();
        }, 500);
    },
    onremove: function(vnode){
        window.clearInterval(this.refresher);
    },
    onupdate: function(vnode){
        if (this.visible && !this.dimensions) {
            util.imageDimensions(this.imageUrl).then(f => {
                this.dimensions = f;
                m.redraw();
            });
        }
    },
    view: function(vnode){
        const dw = vnode.attrs.size;
        const dh = vnode.attrs.size;
        const climbs = vnode.attrs.photo?.climbLayers || [];
        const areas = vnode.attrs.photo?.areaLayers || [];
        const iw = this.dimensions?.width;
        const ih = this.dimensions?.height;
        const dx = (dw - iw) / 2;
        const dy = (dh - ih) / 2;
        return m('svg.photo-thumbnail', {
            height: `${dh}px`,
            width: `${dw}px`,
            viewBox: `0 0 ${dh} ${dw}`,
            // style: {'overflow': 'visible'},
        }, (this.size && this.visible) && [
            m('image', {
                'xlink:href': this.imageUrl,
                x: 0 + dx,
                y: 0 + dy,
                height: ih,
                width: iw,
            }),
            areas.map(a => {
                let pts = a.polygon.map(pt => {
                    let x = pt.leftOffset * iw + dx;
                    let y = pt.topOffset * ih + dy;
                    return x + ' ' + y;
                })
                let d = 'M' + pts.join(' L ');
                return m('path', {
                    d: d,
                    'stroke-width': ((iw+ih)/2) * 0.005,
                    'stroke': 'yellow',
                    'stroke-dasharray': '2 3',
                    'fill': 'none',
                });
            }),
            climbs.map(c => {
                let pts = c.path.map(pt => {
                    let x = pt.leftOffset * iw + dx;
                    let y = pt.topOffset * ih + dy;
                    return x + ' ' + y;
                })
                let d = 'M' + pts.join(' L ');
                return m('path', {
                    d: d,
                    'stroke-width': ((iw+ih)/2) * 0.01,
                    'stroke': 'rgba(255, 255, 100, 0.7)',
                    'stroke-linecap': 'round',
                    'fill': 'none',
                });
            }),
        ]);
    },
};
