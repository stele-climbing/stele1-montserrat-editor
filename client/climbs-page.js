class ClimbsViewModel {
    constructor(){
        this.climbs = [];
        m.request('/climbs').then(a => this.climbs = a);
    }
    listClimbs(){
        return this.climbs.sort((a, b) => a.name.localeCompare(b.name));
    }
}

pages.Climbs = {
    oninit: function(){
        document.title = 'Climbs';
        this.newClimbName = '';
        this.vm = new ClimbsViewModel();
    },
    view: function() {
        return m('article.climbs-page', [
            m('header.climbs-page__header', [
              m('.climbs-page__header-title', [
                m('h1', 'climbs'),
                m('div', [
                  'use ', m('kbd', 'Ctrl'), ' + ', m('kbd', 'f'), ' to search'
                ]),
              ]),
              m('section', [
                m('input', {
                  type: 'text',
                  value: this.newClimbName,
                  placeholder: 'new climb name',
                  oninput: e => this.newClimbName = e.target.value,
                }),
                m('button', {
                  disabled: !this.newClimbName.trim(),
                  onclick: e => {
                    m.request({
                      method: 'POST',
                      url: '/climbs',
                      body: {name: this.newClimbName.trim()}
                    }).then(_ => {
                      m.request('/climbs').then(a => this.climbs = a);
                    });
                  }
                }, 'create'),
              ]),
            ]),
            m('ul.climbs-index', this.vm.listClimbs().map(function(c){ 
                return m('li.climbs-index__item', [
                    m(m.route.Link, {href: '/climbs/' + c.uuid}, [
                      m('span.climb-index__climb', c.name)
                    ])
                ]);
            })),
        ]);
    },
};
