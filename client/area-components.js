window.components.area = {};

window.components.area.Name = {
    view: function(vnode){
        return m('input.area-name', {
            value: vnode.attrs.value,
            placeholder: 'area name',
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.area.UUID = {
    view: function(vnode){
        return m('input', {
            value: vnode.attrs.value,
            placeholder: 'uuid',
            disabled: true,
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.area.AlternateNames = {
    view: function(vnode, a){
        var width = 40;
        let anames = vnode.attrs.value ? vnode.attrs.value.slice() : [];
        return m('div', anames.map(function(n, idx){
            return m('div', [
                m('input', {
                    value: n,
                    size: width,
                    placeholder: 'alternate name',
                    oninput: function(e){
                        anames[idx] = e.target.value;
                        vnode.attrs.onchange(anames);
                    },
                }),
                m('button', {
                    onclick: function(){
                        vnode.attrs.onchange(anames.slice(0,idx).concat(anames.slice(idx+1)))
                    },
                }, 'remove')
            ]);
        }).concat([m('div', [
            m('input', {
                placeholder: 'new alternate name',
                value: '',
                size: width,
                oninput: function(e){
                    if (e.target.value != '') {
                        vnode.attrs.onchange((vnode.attrs.value || []).concat([e.target.value]))
                    }
                },
            }),
        ])]));
    },
};


window.components.area.Fields = {
    oninit: function(vnode){
        this.newField = '';
    },
    newFieldIsValid: function(vnode){
        let keys = vnode.attrs.value ? Object.keys(vnode.attrs.value) : [];
        let keyConflict = 0 <= keys.indexOf(this.newField);
        return (this.newField !== '') && !keyConflict;
    },
    view: function(vnode){
        let kvs =  vnode.attrs.value || {};
        let ks = Object.keys(kvs).sort();
        return m('.area-fields', [
            m('.area-fields--new-field', [
                m('input.area-fields--new-field-input', {
                    value: this.newField,
                    oninput: e => this.newField = e.target.value,
                    placeholder: 'new field'
                }),
                m('button.area-fields--new-field-add', {
                    disabled: !this.newFieldIsValid(vnode),
                    onclick: a => {
                        let o = util.copy(kvs);
                        o[this.newField] = '';
                        this.newField = '';
                        vnode.attrs.onchange(o);
                    }
                }, 'add'),
            ]),
            ks.map(function(k){
                return m('.area-fields--field', {key: k.topic}, [
                    m('input.area-fields--field-name', {
                        value: k,
                        disabled: true,
                    }),
                    m('input.area-fields--field-value', {
                        value: kvs[k],
                        oninput: e => {
                            let kvs2 = util.copy(kvs);
                            kvs2[k] = e.target.value;
                            vnode.attrs.onchange(kvs2);
                        }
                    }),
                    m('button.area-fields--field-value', {
                        onclick: e => {
                            let kvs2 = util.copy(kvs);
                            delete kvs2[k];
                            vnode.attrs.onchange(kvs2);
                        },
                    }, 'remove'),
                ]);
            }),
        ]);
    },
};


window.components.area.Tags = {
    oninit: function(vnode){
        this.newTag = '';
    },
    view: function(vnode){
        let tags = vnode.attrs.value || [];
        return m('.area-tags', [
            m('input.area-tags--new-tag', {
                value: this.newTag,
                placeholder: 'new tag',
                oninput: e => {
                    this.newTag = e.target.value;
                },
                onkeypress: e => {
                    if ( (e.keyCode === 13) && (this.newTag !== '') ) {
                        vnode.attrs.onchange([this.newTag].concat(tags.filter(t => t !== this.newTag)));
                        this.newTag = '';
                    }
                },
            }),
            m('ul.area-tags--tags', tags.map(function(t){
                return m('li.area-tags--tag', [
                   m('span.area-tags--tag-content', t),
                   m('button.area-tags--remove-tag', {
                       onclick: e => vnode.attrs.onchange(tags.filter(a => a !== t)),
                   }, 'x'),
                ]);
            })),
        ]);
    },
};

window.components.area.Notes = {
    oninit: function(vnode){
        this.newTopic = '';
    },
    newFieldIsValid: function(vnode){
        let keys = vnode.attrs.value ? Object.keys(vnode.attrs.value) : [];
        let keyConflict = 0 <= keys.indexOf(this.newTopic);
        return (this.newTopic !== '') && !keyConflict;
    },
    view: function(vnode){
        let notes = vnode.attrs.value || [];
        return m('.area-notes', [
            m('.area-notes--new-note', [
                m('input.area-notes--new-note-input', {
                    value: this.newTopic,
                    oninput: e => this.newTopic = e.target.value,
                    placeholder: 'new note'
                }),
                m('button.area-notes--new-note-add', {
                    disabled: !this.newFieldIsValid(vnode),
                    onclick: e => {
                        let n2 = [{topic: this.newTopic, content: ''}].concat(util.copy(notes));
                        this.newTopic = '';
                        vnode.attrs.onchange(n2);
                    }
                }, 'add'),
            ]),
            notes.map(function(n){
                return m('.area-notes--note', [
                    m('input.area-notes--note-topic', {
                        value: n.topic,
                        disabled: true,
                    }),
                    m('button.area-notes--remove-note', {
                        onclick: e => {
                            let notes2 = util.copy(notes);
                            vnode.attrs.onchange(notes2.filter(a => a.topic !== n.topic));
                        },
                    }, 'remove'),
                    m('br'),
                    m('textarea.area-notes--note-body', {
                        value: n.content,
                        rows: 5,
                        cols: 50,
                        oninput: e => {
                            let notes2 = util.copy(notes).map(function(a){
                                if (a.topic === n.topic) {
                                    return {topic: a.topic, content: e.target.value};
                                } else {
                                    return a;
                                }
                            });
                            vnode.attrs.onchange(notes2);
                        }
                    }),
                ]);
            }),
        ]);
    },
};

window.components.area.Location = {
    oninit: function(vnode){
        this.inputId = `area-location-selector_${util.serial()}`;
        this.category = 'perimeter';
    },
    perimeterSetter: function(vnode){
        return m(common.PolygonSetter, {
            getPolygon: function(){
                let v = vnode.attrs.value;
                return v ? v.polygon.map(p => [p.longitude, p.latitude]) : null;
            },
            setPolygon: function(poly){
                vnode.attrs.onchange(poly ? {
                    "type": 'perimeter',
                    "polygon": poly[0].map(p => ({
                        longitude: p[0],
                        latitude: p[1],
                    }))
                } : null);
            },
        });
    },
    approximationSetter: function(vnode){
        return m(common.PointAccuracySetter, {
            getCircle: function(){
                let v = vnode.attrs.value?.circle;
                return v ? {
                    point: [v.longitude, v.latitude],
                    accuracy: v.meterRadius,
                } : null;
            },
            setCircle: c => vnode.attrs.onchange(c ? {
                type: 'approximation',
                circle: {
                    longitude: c.point[0],
                    latitude: c.point[1],
                    meterRadius: c.accuracy,
                }
            } : null),
        });
    },
    view: function(vnode){
        if (vnode.attrs.value) this.category = vnode.attrs.value.type;
        return m('.area-location', [
            m('.area-location__map', {style: {height: '400px'}}, [
                (this.category === 'approximation')
                ? this.approximationSetter(vnode)
                : this.perimeterSetter(vnode)
            ]),
            m('.area-location__controls', {style:{textAlign: 'right'}}, [
                m('button', {
                  disabled: this.category === 'perimeter',
                  onclick: () => {
                      if (this.category === 'perimeter') return null;
                      this.category = 'perimeter';
                      vnode.attrs.onchange(null);
                  },
                }, (this.category === 'perimeter') ? 'setting perimeter' : 'switch to perimeter'),
                m('button', {
                  disabled: this.category === 'approximation',
                  onclick: () => {
                      if (this.category === 'approximation') return null;
                      this.category = 'approximation';
                      vnode.attrs.onchange(null);
                  },
                }, (this.category === 'approximation') ? 'setting approximation' : 'switch to approximation'),
            ]),
        ]);
    },
};
