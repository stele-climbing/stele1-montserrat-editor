window.components.approach = {};

window.components.approach.Name = {
    view: function(vnode){
        return m('input.approach-name', {
            value: vnode.attrs.value,
            placeholder: 'approach name',
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.approach.Description = {
    view: function(vnode){
        return m('textarea.approach-name', {
            value: vnode.attrs.value,
            rows: 3,
            cols: 50,
            placeholder: 'approach description',
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.approach.UUID = {
    view: function(vnode){
        return m('input.approach-uuid', {
            value: vnode.attrs.value,
            placeholder: 'uuid',
            disabled: true,
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.approach.Path = {
    view: function(vnode){
        return m('.approach-path', {style: {height: '400px'}}, [
           m(common.PathSetter, {
               getPath: function(){
                   let v = vnode.attrs.value;
                   return v ? v.map(pt => [pt.longitude, pt.latitude]) : v;
               },
               setPath: function(pat){
                   let named = pat.map(pt => ({longitude: pt[0], latitude: pt[1]}));
                   vnode.attrs.onchange(named);
               }
           }),
        ]);
    },
};
