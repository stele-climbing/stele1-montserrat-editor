pages.Photo = {
    oninit: function(vnode){
        this.photo = null;
        this.climbs = [];
        this.areas = [];
        m.request('/photos/' + vnode.attrs.id).then(a => this.photo = a);
        m.request('/photos/' + vnode.attrs.id + '/exif').then(a => this.exif = a);
        m.request('/climbs').then(a => this.climbs = a);
        m.request('/areas').then(a => this.areas = a);
    },
    save: util.debounce(1000, function(){
        Object.keys(this.photo).forEach(k => {
            let v = this.photo[k];
            if (v === undefined || v === null) delete this.photo[k];
        });
        m.request({
            method: 'PUT',
            url: '/photos/:id',
            params: {id: this.photo.uuid},
            body: this.photo,
        });
    }),
    view: function(vnode) {
        document.title = '📷 ' + this.photo?.uuid;
        return !(this.photo && this.exif) ? m('p', 'loading') : m('article.photo-page', [
            m('.photo-page__upper', [
                m('figure.photo-page__thumbnail-figure', [
                    m('img.image', {src: '/photos/' + this.photo.uuid + '.jpg?covering_square=500'}),
                    m('figcaption', [
                        m('h1.small', `photo ${this.photo.uuid}`),
                        m('span', [
                            m('a', {
                              href: '/photos/' + this.photo.uuid + '.jpg',
                              target: '_blank',
                            }, 'full size'),
                            ' | ',
                            m('a', {href: `/photos/${vnode.attrs.id}/exif`}, 'exif data'),
                        ]),
                    ])
                ]),
                m('section.photo-page__text-attributes', [
                    m('.uuid', m(components.photo.UUID, {
                        value: this.photo.uuid
                    })),
                    m('.fields', m(components.photo.Fields, {
                        value: this.photo.fields,
                        onchange: v => { this.photo.fields = v; this.save() },
                    })),
                    m('.real-orientation', m(components.photo.RealOrientation, {
                        value: this.photo.realOrientation,
                        onchange: a => { this.photo.realOrientation = a; this.save() },
                    })),
                    m('.real-datetimeoriginal', m(components.photo.RealDatetimeoriginal, {
                        value: this.photo.realDatetimeoriginal,
                        onchange: v => { this.photo.realDatetimeoriginal = v; this.save() },
                    })),
                ]),
            ]),
            m('.real-location', [
                m(components.photo.RealLocation, {
                    value: this.photo.realLocation,
                    onchange: v => { this.photo.realLocation = v; this.save() },
                    suggestedBounds: util.roughBounds.getAsLeafletBounds(),
                    suggestedCenter: this.exif?.location ? {
                        point: this.exif.location,
                        popupContent: "GPS location from the photo's EXIF data"
                    } : null,
                })
            ]),
            m('.climb-layers', m(components.photo.ClimbLayers, {
              climbs: this.climbs,
              photoUuid: this.photo.uuid,
              value: this.photo.climbLayers,
              onchange: a => { this.photo.climbLayers = a; this.save() },
            })),
            m('.area-layers', m(components.photo.AreaLayers, {
              areas: this.areas,
              photoUuid: this.photo.uuid,
              value: this.photo.areaLayers,
              onchange: a => { this.photo.areaLayers = a; this.save() },
            })),
            m('details.photo-data-summary', {closed: 'closed'}, [
                m('summary', 'photo-data'),
                m('.photo-data-summary__json', {
                    style: {
                        border: 'solid 1px black',
                        'white-space': 'pre',
                        padding: '1em',
                        overflowX: 'scroll'
                    }
                }, JSON.stringify(this.photo, null, 2)),
            ]),
            m('button', {
              style: {color: 'white', background: 'red'},
              onclick: () => {
                m.request({
                  url: '/photos/' + this.photo.uuid,
                  method: 'DELETE',
                }).then(() => m.route.set('/photos'));
              }
            }, 'delete photo'),
        ]);
    },
};

