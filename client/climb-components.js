window.components.climb = {};

window.components.climb.Name = {
    view: function(vnode){
        return m('input.climb-name-input', {
            value: vnode.attrs.value,
            placeholder: 'climb name',
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.climb.UUID = {
    view: function(vnode){
        return m('input.climb-uuid-input', {
            value: vnode.attrs.value,
            placeholder: 'uuid',
            disabled: true,
            oninput: e => vnode.attrs.onchange(e.target.value),
        });
    },
};

window.components.climb.AlternateNames = {
    view: function(vnode, a){
        var width = 30;
        let anames = vnode.attrs.value ? vnode.attrs.value.slice() : [];
        return m('div.climb-alternate-names-input', anames.map(function(n, idx){
            return m('div', [
                m('input', {
                    value: n,
                    size: width,
                    placeholder: 'alternate name',
                    oninput: function(e){
                        anames[idx] = e.target.value;
                        vnode.attrs.onchange(anames);
                    },
                }),
                m('button', {
                    onclick: function(){
                        vnode.attrs.onchange(anames.slice(0,idx).concat(anames.slice(idx+1)))
                    },
                }, 'remove')
            ]);
        }).concat([m('div', [
            m('input', {
                placeholder: 'new alternate name',
                value: '',
                size: width,
                oninput: function(e){
                    if (e.target.value != '') {
                        vnode.attrs.onchange((vnode.attrs.value || []).concat([e.target.value]))
                    }
                },
            }),
        ])]));
    },
};

window.components.climb.Ascents = {
    view: function(vnode){
        var width = 40;
        let ascents = vnode.attrs.value ? vnode.attrs.value.slice() : [];
        return m('div', ascents.map(function(a, idx){
            return m('div', [
                m('input', {
                    value: a,
                    size: width,
                    placeholder: 'ascent',
                    oninput: function(e){
                        ascents[idx] = e.target.value;
                        vnode.attrs.onchange(ascents);
                    },
                }),
                m('button', {
                    onclick: function(){
                        vnode.attrs.onchange(ascents.slice(0,idx).concat(ascents.slice(idx+1)))
                    },
                }, 'remove')
            ]);
        }).concat([m('div', [
            m('input', {
                placeholder: 'add significant ascents',
                value: '',
                size: width,
                oninput: function(e){
                    if (e.target.value != '') {
                        vnode.attrs.onchange((vnode.attrs.value || []).concat([e.target.value]))
                    }
                },
            }),
        ])]));
    },
};

window.components.climb.Fields = {
    oninit: function(vnode){
        this.newField = '';
    },
    newFieldIsValid: function(vnode){
        let keys = vnode.attrs.value ? Object.keys(vnode.attrs.value) : [];
        let keyConflict = 0 <= keys.indexOf(this.newField);
        return (this.newField !== '') && !keyConflict;
    },
    view: function(vnode){
        let kvs =  vnode.attrs.value || {};
        let ks = Object.keys(kvs).sort();
        return m('.climb-fields-input', [
            m('.climb-fields-input--new-field', [
                m('input.climb-fields-input--new-field-input', {
                    value: this.newField,
                    oninput: e => this.newField = e.target.value,
                    placeholder: 'new field'
                }),
                m('button.climb-fields-input--new-field-add', {
                    disabled: !this.newFieldIsValid(vnode),
                    onclick: a => {
                        let o = util.copy(kvs);
                        o[this.newField] = '';
                        this.newField = '';
                        vnode.attrs.onchange(o);
                    }
                }, 'add'),
            ]),
            ks.map(function(k){
                return m('.climb-fields-input--field', {key: k.topic}, [
                    m('input.climb-fields-input--field-key', {
                        value: k,
                        disabled: true,
                    }),
                    m('input.climb-fields-input--field-value', {
                        value: kvs[k],
                        oninput: e => {
                            let kvs2 = util.copy(kvs);
                            kvs2[k] = e.target.value;
                            vnode.attrs.onchange(kvs2);
                        }
                    }),
                    m('button.climb-fields-input--field-remove', {
                        onclick: e => {
                            let kvs2 = util.copy(kvs);
                            delete kvs2[k];
                            vnode.attrs.onchange(kvs2);
                        },
                    }, 'remove'),
                ]);
            }),
        ]);
    },
};

window.components.climb.Resources = {
    oninit: function(vnode){
      this.newLink = '';
    },
    view: function(vnode){
        let rs = vnode.attrs.value || [];
        return m('.climb-resources', [
            rs.map(function(r, i){
                return m('.climb-resource-input', {
                    style: {
                        background: r.resource ? 'none' : 'red'
                    }
                }, [
                    m('input.climb-resource-input--resource', {
                        placeholder: 'URL or URI or Link',
                        value: r.resource,
                        size: 40,
                        oninput: e => {
                            let rs2 = util.copy(rs);
                            rs2[i].resource = e.target.value;
                            vnode.attrs.onchange(rs2);
                        }
                    }),
                    m('input.climb-resource-input--title', {
                        placeholder: 'title (recommended)',
                        value: r.title,
                        size: 40,
                        oninput: e => {
                            let rs2 = util.copy(rs);
                            rs2[i].title = e.target.value;
                            vnode.attrs.onchange(rs2);
                        }
                    }),
                    m('textarea.climb-resource-input--description', {
                        placeholder: 'description (optional)',
                        value: r.description,
                        cols: 30,
                        rows: 1,
                        oninput: e => {
                            let rs2 = util.copy(rs);
                            rs2[i].description = e.target.value;
                            vnode.attrs.onchange(rs2);
                        }
                    }),
                    m('button.climb-resource-input--remove', {
                        disabled: !r.resource.trim(),
                        onclick: e => {
                            let rs2 = [];
                            rs.forEach((r2,i2) => { if (i !== i2) rs2.push(util.copy(r2)) });
                            vnode.attrs.onchange(rs2);
                        },
                    }, 'remove'),
                ]);
            }),
            m('.climb-resources--new', [
                m('input.climb-resource-input--resource', {
                    placeholder: 'URL or URI or Link',
                    value: this.newLink,
                    oninput: e => { this.newLink = e.target.value; }
                }),
                m('button.climb-resources-input--create', {
                    disabled: !this.newLink.trim(),
                    onclick: e => {
                        let rs2 = util.copy(rs);
                        rs2.push({resource: this.newLink});
                        this.newLink = '';
                        vnode.attrs.onchange(rs2);
                    },
                }, 'add'),
            ])
        ]);
    },
};


window.components.climb.Length = {
    oninit: function(vnode){
        this.working = null;
        this.refreshWorking(vnode);
    },
    // use a working value because we can't send bad values to the parent
    refreshWorking(vnode){
        let current = util.copy(vnode.attrs.value);
        let last = this.lastSeen;
        // if both are falsy, no update needed
        if (!current && !last) {
            return;
        // if one is truthy, update
        } else if ( (!!current && !last) ||
                    (!current && !!last) ) {
            this.lastSeen = current;
            this.working = util.copy(current);
            return;
        // both are truthy, so check details before updating
        } else if ( (last.lowerEstimate !== current.lowerEstimate) ||
                    (last.upperEstimate !== current.upperEstimate) ||
                    (last.unit !== current.unit) ) {
            this.lastSeen = current;
            this.working = util.copy(current);
            return;
        }
    },
    workingIsValid: function(vnode){
        let w = this.working;
        return (w === null) ||
               ( (w.lowerEstimate <= w.upperEstimate) &&
                 (w.unit !== '') );
    },
    setWorking: function(vnode, lower, upper, unit){
        let asEndpoint = v => (typeof v  === 'number') ? v : (v == '') ? null : Number(v);
        this.working = (lower || upper || unit) ? {
            lowerEstimate: asEndpoint(lower),
            upperEstimate: asEndpoint(upper),
            unit: unit,
        } : null;
        if (this.workingIsValid(vnode)) {
            vnode.attrs.onchange(util.copy(this.working));
        }
    },
    view: function(vnode){
        this.refreshWorking(vnode);
        let lower = this.working ? this.working.lowerEstimate : '';
        let upper = this.working ? this.working.upperEstimate : '';
        let unit = this.working ? this.working.unit : '';
        return m('.climb-length-input', {
            style: {
                backgroundColor: this.workingIsValid(vnode) ? 'transparent' : 'rgba(255, 0, 0, 0.5)',
            }
        }, [
            m('input.climb-length-input--lower', {
                placeholder: 'lower estimate',
                type: 'number',
                value: this.working ? this.working.lowerEstimate : '',
                size: 10,
                oninput: e => this.setWorking(vnode, e.target.value, upper, unit)
            }),
            m('input.climb-length-input--upper', {
                placeholder: 'upper estimate',
                type: 'number',
                size: 10,
                value: this.working ? this.working.upperEstimate : '',
                oninput: e => this.setWorking(vnode, lower, e.target.value, unit)
            }),
            m('input.climb-length-input--unit', {
                placeholder: 'unit',
                size: 10,
                value: this.working ? this.working.unit : '',
                oninput: e => this.setWorking(vnode, lower, upper, e.target.value)
            }),
            // this.workingIsValid(vnode) ? 'valid' : 'bad',
        ]);
    },
};

window.components.climb.ProjectStatus = {
    view: function(vnode){
        function currentMonthAndYear(){
            const d = new Date();
            const months = ["January", "February", "March",
                            "April", "May", "June", "July",
                            "August", "September", "October",
                            "November", "December"];
            return months[d.getMonth()] + ' ' + d.getFullYear()
        }
        return m('.climb-project-status', [
            m('input', {
              type: 'checkbox',
              checked: (typeof vnode.attrs.value === 'string'),
              onchange: function(e){
                 if (e.target.checked) {
                     vnode.attrs.onchange('as of ' + currentMonthAndYear());
                 } else {
                     vnode.attrs.onchange(null);
                 }
              }
            }),
            (typeof vnode.attrs.value === 'string') ? [
                m('span', ' project '),
                m('input', {
                    size: 35,
                    value: vnode.attrs.value,
                    placeholder: 'project status',
                    oninput: e => vnode.attrs.onchange(e.target.value || null),
                })
            ] : m('span', '(not a project)'),
        ]);
    },
};

window.components.climb.Rating = {
    setRating: function(vnode, d, p, s){
        let r = {};
        if (d) r.difficulty = d;
        if (p) r.protection = p;
        if (s) r.style = s;
        vnode.attrs.onchange(Object.keys(r).length ? r : null)
    },
    view: function(vnode){
        let r = vnode.attrs.value || {};
        let difficulty = r.difficulty;
        let protection = r.protection;
        let style = r.style;
        return m('.climb-rating', [
            m('input.climb-rating--difficulty', {
                value: difficulty,
                size: 5,
                placeholder: 'difficulty',
                oninput: e => this.setRating(vnode, e.target.value, protection, style),
            }),
            m('input.climb-rating--protection', {
                value: protection,
                size: 5,
                placeholder: 'safety rating',
                oninput: e => this.setRating(vnode, difficulty, e.target.value, style),
            }),
            m('input.climb-rating--style', {
                style: {marginLeft: '1em'},
                value: style,
                size: 10,
                placeholder: 'style',
                oninput: e => this.setRating(vnode, difficulty, protection, e.target.value),
            }),
        ]);
    },
};

window.components.climb.Tags = {
    oninit: function(vnode){
        this.newTag = '';
    },
    view: function(vnode){
        let tags = vnode.attrs.value || [];
        return m('.climb-tags', [
            m('input.climb-tags--new-tag', {
                value: this.newTag,
                placeholder: 'new tag',
                oninput: e => {
                    this.newTag = e.target.value;
                },
                onkeypress: e => {
                    if ( (e.keyCode === 13) && (this.newTag !== '') ) {
                        vnode.attrs.onchange([this.newTag].concat(tags.filter(t => t !== this.newTag)));
                        this.newTag = '';
                    }
                },
            }),
            m('ul.climb-tags--tags', tags.map(function(t){
                return m('li.climb-tags--tag', [
                   m('span.climb-tags--tag-content', t),
                   m('button.climb-tags--remove-tag', {
                       onclick: e => vnode.attrs.onchange(tags.filter(a => a !== t)),
                   }, 'x'),
                ]);
            })),
        ]);
    },
};

window.components.climb.Notes = {
    oninit: function(vnode){
        this.newTopic = '';
    },
    newFieldIsValid: function(vnode){
        let keys = vnode.attrs.value ? Object.keys(vnode.attrs.value) : [];
        let keyConflict = 0 <= keys.indexOf(this.newTopic);
        return (this.newTopic !== '') && !keyConflict;
    },
    view: function(vnode){
        let notes = vnode.attrs.value || [];
        return m('.climb-notes', [
            m('.climb-notes--new-note', [
                m('input.climb-notes--new-note-input', {
                    value: this.newTopic,
                    oninput: e => this.newTopic = e.target.value,
                    placeholder: 'new note'
                }),
                m('button.climb-notes--new-note-add', {
                    disabled: !this.newFieldIsValid(vnode),
                    onclick: e => {
                        let n2 = [{topic: this.newTopic, content: ''}].concat(util.copy(notes));
                        this.newTopic = '';
                        vnode.attrs.onchange(n2);
                    }
                }, 'add'),
            ]),
            notes.map(function(n){
                return m('.climb-notes--note', [
                    m('input.climb-notes--note-topic', {
                        value: n.topic,
                        disabled: true,
                    }),
                    m('button.climb-notes--remove-note', {
                        onclick: e => {
                            let notes2 = util.copy(notes);
                            vnode.attrs.onchange(notes2.filter(a => a.topic !== n.topic));
                        },
                    }, 'remove'),
                    m('br'),
                    m('textarea.climb-notes--note-body', {
                        value: n.content,
                        rows: 5,
                        cols: 50,
                        oninput: e => {
                            let notes2 = util.copy(notes).map(function(a){
                                if (a.topic === n.topic) {
                                    return {topic: a.topic, content: e.target.value};
                                } else {
                                    return a;
                                }
                            });
                            vnode.attrs.onchange(notes2);
                        }
                    }),
                ]);
            }),
        ]);
    },
};

window.components.climb.Steepness = (function(){
function addPoint(segs, startAt){
    let pts = segs ? util.copy(segs) : [{startingAt: 0, degreesOverhung: 0}];
    return pts.concat([{startingAt: startAt, degreesOverhung: 0}])
              .sort((a, b) => a.startingAt - b.startingAt);
};
function attachHooks(vnode, el){
    let h = vnode.state.size.height;
    let onchange = v => { vnode.attrs.onchange(v); m.redraw(); };
    el.svg
        .on('mouseenter', function(e){ el.hr.attr('visibility', 'visible') })
        .on('mousemove', function(e){
             y = d3.pointer(e)[1];
             el.hr.attr('y1', y).attr('y2', y);
         })
         .on('mouseleave', function(e){
             el.hr.attr('visibility', 'hidden')
         })
         .on('click', function(e){
             let y = d3.pointer(e)[1];
             let a = Math.round((h - y) / h * 100) / 100;
             onchange(addPoint(vnode.attrs.value, a))
         });
}
function updateSvg(vnode, el){
    let size = vnode.state.size;
    let yPosn = d3.scaleLinear([0, 1], [size.height, 0]);
    let v = util.copy(vnode.attrs.value) || [];
    let w = size.width;
    let h = size.height;
    let decorated = []
    for (let i = 0; i < (v.length-1); i++) {
        decorated.push({
            degreesOverhung: v[i].degreesOverhung,
            startingAt: v[i].startingAt,
            endingAt: v[i+1].startingAt,
            len: v[i+1].startingAt - v[i].startingAt,
            midpoint: ((v[i+1].startingAt - v[i].startingAt) / 2) + v[i].startingAt,
        });
    }
    if (v.length) {
        decorated.push({
            startingAt: v[v.length-1].startingAt,
            degreesOverhung: v[v.length-1].degreesOverhung,
            endingAt: 1,
            len: 1 - v[v.length-1].startingAt,
            midpoint: ((1 - v[v.length-1].startingAt) / 2) + v[v.length-1].startingAt,
        });
    }
    decorated.forEach(function(seg){ // sorry for this
        let radians = (seg.degreesOverhung / 180) * Math.PI + Math.PI;
        seg.rise = Math.sin(radians);
        seg.run = Math.cos(radians);
    });
    let segs = el.segmentsg.selectAll('g.segment-block').data(decorated);
    segs.enter().append('g').classed('segment-block', true).call(function(sel){
        sel.append('circle').classed('midpoint', true).attr('cx', w/2).attr('r', 2);
        sel.append('line').classed('ray', true)
            .attr('stroke', 'black').attr('stroke-width', 1)
        sel.append('line').classed('segment', true)
            .attr('stroke-dasharray', '1 1').attr('stroke', 'gray').attr('stroke-width', 1)
            .attr('x1', w/2).attr('x2', w/2);
    });
    let segsUpdate = el.segmentsg.selectAll('g.segment-block').data(decorated);
    segsUpdate.enter().merge(segsUpdate).call(function(sel){
        sel.select('circle.midpoint').attr('cy', d => yPosn(d.midpoint));
        sel.select('line.ray')
            .attr('x1', w/2)
            .attr('y1', d => yPosn(d.midpoint))
            .attr('x2', d => w/2+(d.rise*(w/4)))
            .attr('y2', d => yPosn(d.midpoint)+(d.run*(w/4)));
        sel.select('line.segment')
            .attr('y1', d => yPosn(d.startingAt) - 2)
            .attr('y2', d => yPosn(d.endingAt) + 2);
    });
    segs.exit().remove();
};
return {
    oninit: function(vnode){
        this.size = {width: 50, height: 200};
    },
    view: function(vnode){
        let v = util.copy(vnode.attrs.value) || [];
        let w = this.size.width;
        let h = this.size.height;
        return m('.climb-steepness', {style: {border: 'solid 1px black'}}, [
           m('svg', {
               oncreate: function(vn){
                   let svg = d3.select(vn.dom)
                     .style('width', w + 'px')
                     .style('height', h + 'px')
                     .style('border', 'solid 1px black')
                     .attr('viewBox', `0 0 ${w} ${h}`);
                   svg.append('text').attr('x', 0).attr('y', h).style('stroke', 'black').text('🧍');
                   let hr = svg.append('line').attr('x1', 0).attr('x2', w).style('stroke', 'gray').style('stroke-width', 1);
                   let segmentsg = svg.append('g').classed('segments', true);
                   this.elements = {svg, hr, segmentsg};
                   updateSvg(vnode, this.elements);
                   attachHooks(vnode, this.elements);
               },
               onupdate: function(vn){
                   updateSvg(vnode, this.elements);
                   attachHooks(vnode, this.elements);
               },
           }),
           m('ul', util.reverse(v).map(function(seg){
               return m('li', {key: seg.startingAt}, [
                   m('label', [
                      m('span', (seg.startingAt * 100) + '%'),
                      m('input', {
                          value: seg.degreesOverhung,
                          type: 'number',
                          oninput: function(e){
                              let val = e.target.value;
                              vnode.attrs.onchange(v.map(function(a){
                                  return a.startingAt === seg.startingAt ? {
                                      degreesOverhung: util.looksLikeNumber(val) ? Number(val) : val,
                                      startingAt: a.startingAt,
                                  } : a;
                              }).sort((a, b) => a.startingAt - b.startingAt));
                          },
                      }),
                   ])
               ]);
           })),
           m('button', {
             onclick: _ => vnode.attrs.onchange([{startingAt: 0, degreesOverhung: 0}])
           }, 'start from 0'),
           m('button', {
             onclick: _ => vnode.attrs.onchange(null)
           }, 'clear'),
        ]);
    },
};
})();

window.components.climb.Terrain = (function(){
function decorateSegments(segs){
    if (!segs) return [];
    let decorated = []
    for (let i = 0; i < (segs.length-1); i++) {
        decorated.push({
            description: segs[i].description,
            startingAt: segs[i].startingAt,
            endingAt: segs[i+1].startingAt,
            len: segs[i+1].startingAt - segs[i].startingAt,
            midpoint: ((segs[i+1].startingAt - segs[i].startingAt) / 2) + segs[i].startingAt,
        });
    }
    if (segs.length) {
        decorated.push({
            startingAt: segs[segs.length-1].startingAt,
            description: segs[segs.length-1].description,
            endingAt: 1,
            len: 1 - segs[segs.length-1].startingAt,
            midpoint: ((1 - segs[segs.length-1].startingAt) / 2) + segs[segs.length-1].startingAt,
        });
    }
    return decorated;
};
function addPoint(segs, startAt){
    let pts = segs.length ? util.copy(segs) : [{startingAt: 0, description: '???'}];
    return pts.concat([{startingAt: startAt, description: '???'}])
              .sort((a, b) => a.startingAt - b.startingAt);
};
function updateSvg(vnode, el){
    let leftEdge = vnode.state.size.leftEdge;
    let yPosn = d3.scaleLinear([0, 1], [vnode.state.size.height, 0]);
    let h = vnode.state.size.height;
    el.svg.on('mouseenter', function(e){
            el.hr.attr('visibility', 'visible')
        })
        .on('mousemove', function(e){
             y = d3.pointer(e)[1];
             el.hr.attr('y1', y).attr('y2', y);
         })
         .on('mouseleave', function(e){
             el.hr.attr('visibility', 'hidden')
         })
         .on('click', function(e){
             let y = d3.pointer(e)[1];
             let a = Math.round((h - y) / h * 100) / 100;
             vnode.attrs.onchange(addPoint(vnode.attrs.value, a));
             m.redraw();
         });
    let decorated = decorateSegments(vnode.attrs.value);
    let segs = el.segmentsg.selectAll('g.segment-block').data(decorated);
    segs.enter().append('g').classed('segment-block', true).call(function(sel){
        sel.append('circle').classed('midpoint', true).attr('cx', leftEdge).attr('r', 2);
        sel.append('text').classed('description', true)
            .attr('stroke', 'black').attr('stroke-width', 1)
            .attr('font-size', 9)
            .attr('x', leftEdge + 5);
        sel.append('line').classed('segment', true)
            .attr('stroke-dasharray', '1 1').attr('stroke', 'gray').attr('stroke-width', 1)
            .attr('x1', leftEdge).attr('x2', leftEdge);
    });
    let segsUpdate = el.segmentsg.selectAll('g.segment-block').data(decorated);
    segsUpdate.enter().merge(segsUpdate).call(function(sel){
        sel.select('circle.midpoint').attr('cy', d => yPosn(d.midpoint));
        sel.select('text.description')
            .attr('y', d => yPosn(d.midpoint))
            .text(d => d.description);
        sel.select('line.segment')
            .attr('y1', d => yPosn(d.startingAt) - 2)
            .attr('y2', d => yPosn(d.endingAt) + 2);
    });
    segs.exit().remove();
};
return {
    oninit: function(vnode){
        this.size = {width: 200, height: 200, leftEdge: 25};
    },
    view: function(vnode){
        let v = util.copy(vnode.attrs.value) || [];
        return m('.climb-terrain', {style: {border: 'solid 1px black'}}, [
           m('svg', {
               oncreate: function(vn){
                   let w = vnode.state.size.width;
                   let h = vnode.state.size.height;
                   let svg = d3.select(vn.dom)
                     .style('width', w + 'px')
                     .style('height', h + 'px')
                     .style('border', 'solid 1px black')
                     .attr('viewBox', '0 0 ' + w + ' ' + h);
                   let hr = svg.append('line').attr('x1', 0).attr('x2', w).style('stroke', 'gray').style('stroke-width', 1);
                   let segmentsg = svg.append('g').classed('segments', true);
                   this.elements = {svg, hr, segmentsg};
                   updateSvg(vnode, this.elements);
               },
               onupdate: function(vn){
                   updateSvg(vnode, this.elements);
               },
           }),
           m('ul', util.reverse(v).map(function(seg){
               return m('li', {key: seg.startingAt}, [
                   m('label', [
                      m('span', (seg.startingAt * 100) + '%'),
                      m('input', {
                          value: seg.description,
                          style: {
                            backgroundColor: seg.description === '' ? 'rgba(255, 0, 0, 0.5)' : 'unset',
                          },
                          oninput: function(e){
                              vnode.attrs.onchange(v.map(function(a){
                                  return a.startingAt === seg.startingAt ? {
                                      description: e.target.value,
                                      startingAt: a.startingAt,
                                  } : a;
                              }).sort((a, b) => a.startingAt - b.startingAt));
                          },
                      }),
                   ])
               ]);
           })),
           m('button', {
             onclick: _ => vnode.attrs.onchange([{startingAt: 0, description: '???'}])
           }, 'start from 0'),
           m('button', {
             onclick: _ => vnode.attrs.onchange(null)
           }, 'clear'),
        ]);
    },
};
})();

window.components.climb.Location = {
    view: function(vnode){
        return m('.climb-location', {style: {height: '400px'}}, [
           m(common.PointAccuracySetter, {
               getCircle: function(){
                   let v = vnode.attrs.value;
                   return v ? {
                       point: [v.longitude, v.latitude],
                       accuracy: v.meterRadius,
                   } : null;
               },
               setCircle: c => vnode.attrs.onchange(c ? {
                   longitude: c.point[0],
                   latitude: c.point[1],
                   meterRadius: c.accuracy,
               } : null),
               suggestedCenter: vnode.attrs.suggestedCenter,
               suggestedBounds: vnode.attrs.suggestedBounds,
           }),
        ]);
    },
};
