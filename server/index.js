const http = require('http');
const path = require('path');
const fs = require('fs');
const child_process = require('child_process');

const sfs = require('@stele1/filesystem');
const metadata = require('@stele1/datatypes/metadata');
const climb = require('@stele1/datatypes/climb');
const area = require('@stele1/datatypes/area');
const parking = require('@stele1/datatypes/parking');
const approach = require('@stele1/datatypes/approach');
const photo = require('@stele1/datatypes/photo');

// from https://www.tutorialspoint.com/how-to-create-guid-uuid-in-javascript
function createUUID() {
   return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
   });
}

class ExifData {
    constructor(imgpath){
        this.textFields = {};
        let stdout = child_process.execSync(`identify -format '%[EXIF:*]' ${imgpath}`, {encoding: 'utf8'});
        stdout.split('\n').forEach(line => {
            if (!line) return;
            let m = line.match(/exif:([^=]*)=(.*)/);
            if (m) this.textFields[m[1]] = m[2];
        });
    }
    getGpsLatLng(){
        function readMeasure(str){
            let dms = str.split(', ').map(a => a.split('/').map(b => Number(b)));
            let d = dms[0][0] / dms[0][1];
            let m = dms[1][0] / dms[1][1];
            let s = dms[2][0] / dms[2][1];
            return d + (m/60) + (s/60/60);
        }
        let lat = this.textFields.GPSLatitude;
        let latRef = this.textFields.GPSLatitudeRef;
        let lng = this.textFields.GPSLongitude;
        let lngRef = this.textFields.GPSLongitudeRef;
        if (lat && latRef && lng && lngRef) {
             return {
                 latitude:  readMeasure(lat) * (latRef === 'N' ? 1 : -1),
                 longitude: readMeasure(lng) * (lngRef === 'E' ? 1 : -1)
             }
        } else {
             return null;
        }
    }
    getDateTimeOriginal(){
        if (!this.textFields.hasOwnProperty('DateTimeOriginal')) return null;
        let dt = this.textFields.DateTimeOriginal.split(' ');
        let ymd = dt[0].split(':');
        let y = ymd[0];
        let m = ymd[1];
        let d = ymd[2];
        let t = dt[1];
        return `${y}-${m}-${d} ${t}`;
    }
    getOrientation(){
        if (this.textFields.hasOwnProperty('Orientation'))
            return Number(this.textFields.Orientation);
        else
            return null;
    }
}

//TODO: use url
let reqPath = req => req.url.split('#')[0].split('?')[0]
let reqPathSegments = req => reqPath(req).replace(/^\//, '').split('/');

const reqMatches = function(req, httpMethod, pathPattern){
    if (req.method === httpMethod) {
        let pattern = new RegExp('^' + pathPattern.replace('*', '[^/]*') + '$');
        return reqPath(req).match(pattern);
    }
    return false;
};

const guessMimeType = function(fpath){
    switch (path.extname(fpath)) {
        case '.js':    return 'application/javascript';
        case '.json':  return 'application/json';
        case '.css':   return 'text/css';
        case '.html':  return 'text/html';
        case '.jpeg':
        case '.jpg':   return 'image/jpg';
        case '.png':   return 'image/png';
        case '.svg':   return 'image/svg+xml';
        case '.ico':   return 'image/x-icon';
        default:       return 'application/octet-stream';
    }
};

const getMetadata = function(req, res, store){
    let md = store.getMetadata()
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.write(JSON.stringify(md.toData()));
    res.end();
};

const setMetadata = function(req, res, store){
    readBodyJSON(req).then(function(j){
        store.setMetadata(metadata.Metadata.fromData(j));
        res.writeHead(204, {'Content-Location': '/metadata'});
        res.end();
    });
};

const listClimbs = function(req, res, store){
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(store.climbUuids().map(a => store.getClimbByUuid(a).toData())));
    res.end();
};

const getClimb = function(req, res, store){
    let cid = new climb.Uuid(reqPathSegments(req)[1]);
    let c = store.getClimbByUuid(cid);
    if (c) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(c.toData()));
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'climb not found'}));
        res.end();
    }
};

const createClimb = function(req, res, store){
    readBodyJSON(req).then(function(j){
        const c = climb.Climb.fromData(j);
        const newid = createUUID();
        c.setUuid(climb.Uuid.fromData(newid));
        store.setClimb(c);
        res.writeHead(201, {
            'Content-Type': 'application/json',
            'Location': 'http://' + req.headers.host + '/climbs/' + newid,
        });
        res.end();
    })
};

const removeClimb = function(req, res, store){
    let id = new climb.Uuid(reqPathSegments(req)[1]);
    let a = store.getClimbByUuid(id);
    if (a) {
        store.removeClimbByUuid(id);
        res.writeHead(204, {'Content-Type': 'application/json'});
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'photo not found'}));
        res.end();
    }
};


const setClimb = function(req, res, store){
    let cid = new climb.Uuid(reqPathSegments(req)[1]);
    let c = store.getClimbByUuid(cid);
    if (c) {
        readBodyJSON(req).then(function(j){
            store.setClimb(climb.Climb.fromData(j));
            res.writeHead(204, {'Content-Type': 'application/json'});
            res.end();
        })
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'climb not found'}));
        res.end();
    }
};

const listAreas = function(req, res, store){
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(store.areaUuids().map(a => store.getAreaByUuid(a).toData())));
    res.end();
};

const createArea = function(req, res, store){
    readBodyJSON(req).then(function(j){
        const a = area.Area.fromData(j);
        const newid = createUUID();
        a.setUuid(area.Uuid.fromData(newid));
        store.setArea(a);
        res.writeHead(201, {
            'Content-Type': 'application/json',
            'Location': 'http://' + req.headers.host + '/areas/' + newid,
        });
        res.end();
    })
};

const removeArea = function(req, res, store){
    let id = new area.Uuid(reqPathSegments(req)[1]);
    let a = store.getAreaByUuid(id);
    if (a) {
        store.removeAreaByUuid(id);
        res.writeHead(204, {'Content-Type': 'application/json'});
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'photo not found'}));
        res.end();
    }
};


const getArea = function(req, res, store){
    let cid = new area.Uuid(reqPathSegments(req)[1]);
    let c = store.getAreaByUuid(cid);
    if (c) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(c.toData()));
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'area not found'}));
        res.end();
    }
};

const setArea = function(req, res, store){
    let cid = new area.Uuid(reqPathSegments(req)[1]);
    let c = store.getAreaByUuid(cid);
    if (c) {
        readBodyJSON(req).then(function(j){
            store.setArea(area.Area.fromData(j));
            res.writeHead(204, {'Content-Type': 'application/json'});
            res.end();
        })
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'area not found'}));
        res.end();
    }
};

const listParkings = function(req, res, store){
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(store.parkingUuids().map(a => store.getParkingByUuid(a).toData())));
    res.end();
};

const createParking = function(req, res, store){
    readBodyJSON(req).then(function(j){
        const p = parking.Parking.fromData(j);
        const newid = createUUID();
        p.setUuid(parking.Uuid.fromData(newid));
        store.setParking(p);
        res.writeHead(201, {
            'Content-Type': 'application/json',
            'Location': 'http://' + req.headers.host + '/parkings/' + newid,
        });
        res.end();
    })
};

const removeParking = function(req, res, store){
    let id = new parking.Uuid(reqPathSegments(req)[1]);
    let a = store.getParkingByUuid(id);
    if (a) {
        store.removeParkingByUuid(id);
        res.writeHead(204, {'Content-Type': 'application/json'});
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'photo not found'}));
        res.end();
    }
};

const getParking = function(req, res, store){
    let cid = new parking.Uuid(reqPathSegments(req)[1]);
    let c = store.getParkingByUuid(cid);
    if (c) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(c.toData()));
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'parking not found'}));
        res.end();
    }
};

const setParking = function(req, res, store){
    let cid = new parking.Uuid(reqPathSegments(req)[1]);
    let c = store.getParkingByUuid(cid);
    if (c) {
        readBodyJSON(req).then(function(j){
            store.setParking(parking.Parking.fromData(j));
            res.writeHead(204, {'Content-Type': 'application/json'});
            res.end();
        })
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'parking not found'}));
        res.end();
    }
};

const listApproaches = function(req, res, store){
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(store.approachUuids().map(a => store.getApproachByUuid(a).toData())));
    res.end();
};

const createApproach = function(req, res, store){
    readBodyJSON(req).then(function(j){
        const a = approach.Approach.fromData(j);
        const newid = createUUID();
        a.setUuid(approach.Uuid.fromData(newid));
        store.setApproach(a);
        res.writeHead(201, {
            'Content-Type': 'application/json',
            'Location': 'http://' + req.headers.host + '/approaches/' + newid,
        });
        res.end();
    })
};

const removeApproach = function(req, res, store){
    let id = new approach.Uuid(reqPathSegments(req)[1]);
    let a = store.getApproachByUuid(id);
    if (a) {
        store.removeApproachByUuid(id);
        res.writeHead(204, {'Content-Type': 'application/json'});
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'photo not found'}));
        res.end();
    }
};

const getApproach = function(req, res, store){
    let cid = new approach.Uuid(reqPathSegments(req)[1]);
    let c = store.getApproachByUuid(cid);
    if (c) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(c.toData()));
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'approach not found'}));
        res.end();
    }
};

const setApproach = function(req, res, store){
    let cid = new approach.Uuid(reqPathSegments(req)[1]);
    let c = store.getApproachByUuid(cid);
    if (c) {
        readBodyJSON(req).then(function(j){
            store.setApproach(approach.Approach.fromData(j));
            res.writeHead(204, {'Content-Type': 'application/json'});
            res.end();
        })
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'approach not found'}));
        res.end();
    }
};

const listPhotos = function(req, res, store){
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(store.photoUuids().map(a => store.getPhotoByUuid(a).toData())));
    res.end();
};

let imageMagickOrientationName = v => {
    switch (v) { // untested. if orientation behavior is odd, look here
        case 1: return 'top-left';     // ⮳
        case 2: return 'top-right';    // ⮲
        case 3: return 'bottom-right'; // ⮰
        case 4: return 'bottom-left';  // ⮱
        case 5: return 'left-top';     // ⮶
        case 6: return 'right-top';    // ⮴
        case 7: return 'right-bottom'; // ⮵
        case 8: return 'left-bottom';  // ⮷
    }
};


const PhotoCacheExtension = {
    rootDir: function(store){
        return store._subpath.call(store, 'extensions', 'photo-cache');
    },
    subpath: function(store, segments){
        return path.join.apply(path, [this.rootDir(store)].concat(segments));
    },
    init: function(store){
        console.log('TODO: PhotoCacheExtension.init() should be fixed');
        console.log('      it _should not_ be a singleton');
        let extensionDir = this.rootDir(store);
        if (!fs.existsSync(extensionDir)) {
            fs.mkdirSync(extensionDir, {recursive: true});
            fs.writeFileSync(this.subpath(store, ['.gitignore']), '*\n');
        }
    },
    purgePhotoData: function(store, photoUuid){
        const uuidstr = photoUuid.toData();
        const pdir = this.subpath(store, ['by-photo', uuidstr])
        if (fs.existsSync(pdir)) {
            fs.rmSync(pdir, {recursive: true});
        }
    },
    getExif: function(store, photoUuid){
        const photoPath = store.getPhotoImageByUuid(photoUuid);
        const uuidstr = photoUuid.toData();
        const d = this.subpath(store, ['by-photo', uuidstr])
        const f = this.subpath(store, ['by-photo', uuidstr, 'exif.json']);
        if (!fs.existsSync(d)) fs.mkdirSync(d, {recursive: true});
        if (!fs.existsSync(f)) {
           const exif = new ExifData(photoPath);
           fs.writeFileSync(f, JSON.stringify({
               orientation: exif.getOrientation(),
               dateTimeOriginal: exif.getDateTimeOriginal(),
               location: exif.getGpsLatLng(),
           }, null, 2));
        }
        return JSON.parse(fs.readFileSync(f));
    },
    getCoveringSquare: function(store, photo, size){
        let origFile = store.getPhotoImageByUuid(photo.getUuid());
        let uuidstr = photo.getUuid().toData();
        let pdir = this.subpath(store, ['by-photo', uuidstr, 'covering-square'])
        let sizedFile = this.subpath(store, ['by-photo', uuidstr, 'covering-square', `${size}.jpg`]);
        if (!fs.existsSync(pdir)) fs.mkdirSync(pdir, {recursive: true});
        if (!fs.existsSync(sizedFile)) {
            if (photo.getRealOrientation()) {
                let o = imageMagickOrientationName(photo.getRealOrientation().toData());
                child_process.execSync(`convert "${origFile}" -strip -orient ${o} -resize ^${size}x${size} "${sizedFile}"`);
            } else {
                child_process.execSync(`convert "${origFile}" -auto-orient -strip -resize ^${size}x${size} "${sizedFile}"`);
            }
        }
        return sizedFile;
    },
    getFullSize: function(store, photo){
        let origFile = store.getPhotoImageByUuid(photo.getUuid());
        let uuidstr = photo.getUuid().toData();
        let pdir = this.subpath(store, ['by-photo', uuidstr])
        let fullsize = this.subpath(store, ['by-photo', uuidstr, 'fullsize.jpg']);
        if (!fs.existsSync(pdir)) fs.mkdirSync(pdir, {recursive: true});
        if (!fs.existsSync(fullsize)) {
            if (photo.getRealOrientation()) {
                let o = imageMagickOrientationName(photo.getRealOrientation().toData());
                child_process.execSync(`convert "${origFile}" -strip -orient ${o} "${fullsize}"`);
            } else {
                child_process.execSync(`convert "${origFile}" -auto-orient -strip "${fullsize}"`);
            }
        }
        return fullsize;
    },
};

const createPhoto = function(req, res, store){
    readBodyJSON(req).then(function(j){
        const buff = Buffer.from(j.base64, 'base64');
        const id = createUUID();
        const extn = j.filename.replace(/^[^.]*\./, '');
        // fs.writeFileSync('/tmp/lastupload', buff)
        store.setPhoto(photo.Photo.fromData({
            uuid: id,
        }));
        store.setPhotoImageByUuid(photo.Uuid.fromData(id), buff, {
            extension: extn
        });
        res.writeHead(201, {
            'Content-Type': 'application/json',
            'Location': 'http://' + req.headers.host + '/photos/' + id,
        });
        res.write(JSON.stringify({
            uuid: id,
            url: 'http://' + req.headers.host + '/photos/' + id,
        }));
        res.end();
    })
};

const removePhoto = function(req, res, store){
    let id = new photo.Uuid(reqPathSegments(req)[1]);
    let a = store.getPhotoByUuid(id);
    if (a) {
        PhotoCacheExtension.purgePhotoData(store, id);
        store.removePhotoByUuid(id);
        res.writeHead(204, {'Content-Type': 'application/json'});
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'photo not found'}));
        res.end();
    }
};

const getPhoto = function(req, res, store){
    let u = new URL(req.url, 'http://' + req.headers.host);
    let cid = path.basename(u.pathname, path.extname(u.pathname));
    let cuuid = new photo.Uuid(cid);
    let c = store.getPhotoByUuid(cuuid);
    if (path.extname(u.pathname) === '.jpg') {
        res.writeHead(200, {'Content-Type': 'image/jpg'});
        let pat = null;
        if (u.searchParams.get('covering_square'))
            pat = PhotoCacheExtension.getCoveringSquare(store, c, u.searchParams.get('covering_square'));
        else
            pat = PhotoCacheExtension.getFullSize(store, c);
        let rs = fs.createReadStream(pat);
        rs.on('data', (chunk) => { res.write(chunk); });
        rs.on('end', (chunk) => { res.end(); });
    } else {
        if (c) {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify(c.toData()));
            res.end();
        } else {
            res.writeHead(404, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({message: 'photo not found'}));
            res.end();
        }
    }
};

const getPhotoExifData = function(req, res, store){
    let id = new photo.Uuid(reqPathSegments(req)[1]);
    if (store.getPhotoByUuid(id)) {
        let photoPath = store.getPhotoImageByUuid(id);
        const exif = PhotoCacheExtension.getExif(store, id);
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(exif));
        res.end();
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'photo not found'}));
        res.end();
    }
};

const setPhoto = function(req, res, store){
    let cid = new photo.Uuid(reqPathSegments(req)[1]);
    let c = store.getPhotoByUuid(cid);
    if (c) {
        readBodyJSON(req).then(function(j){
            store.setPhoto(photo.Photo.fromData(j));
            res.writeHead(204, {'Content-Type': 'application/json'});
            res.end();
        })
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({message: 'photo not found'}));
        res.end();
    }
};


const staticFilePath = function(staticPath, req){
    let subpath = req.url === '/' ? 'index.html' : req.url.replace(/^\//, '');
    return staticPath.replace(/\/$/, '') + '/' + subpath;
};

const matchesExistingStaticFile = function(staticPath, req){
    return fs.existsSync(staticFilePath(staticPath, req));
};

const sendStaticFile = function(req, res, staticPath){
    let a = staticFilePath(staticPath, req);
    let content = fs.readFileSync(a);
    res.writeHead(200, {'Content-Type': guessMimeType(a)});
    res.end(content, 'utf-8');
};

const readBodyJSON = function(req){
    if (req.headers['content-type'] && req.headers['content-type'].match(/^application\/json/)) {
        return new Promise(function(resolve, reject){
            let data = [];
            req.on('data', chunk => { data.push(chunk) })
            req.on('end', () => {
                resolve(JSON.parse(data));
            });
        });
    } else {
        return Promise.reject(new Error('content is not JSON'));
    }
};

var projectServer = function(store, staticPath){
    let server = http.createServer(function (req, res) {
        if (matchesExistingStaticFile(staticPath, req)) sendStaticFile(req, res, staticPath) //TODO: move this to the last case

        else if (reqMatches(req, 'GET',     '/metadata'))    getMetadata(req, res, store)
        else if (reqMatches(req, 'PUT',     '/metadata'))    setMetadata(req, res, store)

        else if (reqMatches(req, 'GET',     '/climbs'))    listClimbs(req, res, store)
        else if (reqMatches(req, 'POST',    '/climbs'))    createClimb(req, res, store)
        else if (reqMatches(req, 'GET',     '/climbs/*'))  getClimb(req, res, store)
        else if (reqMatches(req, 'PUT',     '/climbs/*'))  setClimb(req, res, store)
        else if (reqMatches(req, 'DELETE',  '/climbs/*'))  removeClimb(req, res, store)

        else if (reqMatches(req, 'GET',     '/areas'))    listAreas(req, res, store)
        else if (reqMatches(req, 'POST',    '/areas'))    createArea(req, res, store)
        else if (reqMatches(req, 'GET',     '/areas/*'))  getArea(req, res, store)
        else if (reqMatches(req, 'PUT',     '/areas/*'))  setArea(req, res, store)
        else if (reqMatches(req, 'DELETE',  '/areas/*'))  removeArea(req, res, store)

        else if (reqMatches(req, 'GET',     '/parkings'))    listParkings(req, res, store)
        else if (reqMatches(req, 'POST',    '/parkings'))    createParking(req, res, store)
        else if (reqMatches(req, 'GET',     '/parkings/*'))  getParking(req, res, store)
        else if (reqMatches(req, 'PUT',     '/parkings/*'))  setParking(req, res, store)
        else if (reqMatches(req, 'DELETE',  '/parkings/*'))  removeParking(req, res, store)

        else if (reqMatches(req, 'GET',     '/approaches'))    listApproaches(req, res, store)
        else if (reqMatches(req, 'POST',    '/approaches'))    createApproach(req, res, store)
        else if (reqMatches(req, 'GET',     '/approaches/*'))  getApproach(req, res, store)
        else if (reqMatches(req, 'PUT',     '/approaches/*'))  setApproach(req, res, store)
        else if (reqMatches(req, 'DELETE',  '/approaches/*'))  removeApproach(req, res, store)

        else if (reqMatches(req, 'GET',     '/photos'))         listPhotos(req, res, store)
        else if (reqMatches(req, 'POST',    '/photos'))         createPhoto(req, res, store)
        else if (reqMatches(req, 'GET',     '/photos/*/exif'))  getPhotoExifData(req, res, store)
        else if (reqMatches(req, 'GET',     '/photos/*'))       getPhoto(req, res, store)
        else if (reqMatches(req, 'PUT',     '/photos/*'))       setPhoto(req, res, store)
        else if (reqMatches(req, 'DELETE',  '/photos/*'))       removePhoto(req, res, store)
        else {
            res.statusCode = 404;
            res.end('Invalid Request!');
        }
    });
    return server;
};

function runServer(project, staticFilesDir, address, port){
    PhotoCacheExtension.init(project);
    const srv = projectServer(project, staticFilesDir);
    srv.listen(port, address);
    return srv;
}

module.exports = {
    default: runServer,
};
